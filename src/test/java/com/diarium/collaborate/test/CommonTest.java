package com.diarium.collaborate.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.diarium.collaborate.utils.CommonUtils;

public class CommonTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDuration() {
		Long timestamp = Long.valueOf("1502389385000");
		System.out.println(CommonUtils.getDuration(timestamp));
		assertTrue(true);
	}

}
