package com.diarium.collaborate.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.bind.DatatypeConverter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.Link;
import com.diarium.collaborate.utils.NewsfeedUtils;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlMeta;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.ailis.pherialize.Pherialize;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NewsfeedTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreate() {
		fail("Not yet implemented");
	}

	@Test
	public void testEdit() {
		fail("Not yet implemented");
	}

	@Test
	public void testRemove() {
		fail("Not yet implemented");
	}

	@Test
	public void testEncodeTypeContents() {
		String[] typeContents = new String[] {
				"uploads/users/449/posts/_%d_65237fe755c10fff98eed3c9437a5b38.jpg",
				"uploads/users/449/posts/_%d_40c7e3bd4b827231525203252e860305.jpg"
		};
		String encoded = "YToyOntpOjA7czo2NDoidXBsb2Fkcy91c2Vycy80NDkvcG9zdHMvXyVkXzY1MjM3ZmU3NTVjMTBmZmY5OGVlZDNjOTQzN2E1YjM4LmpwZyI7aToxO3M6NjQ6InVwbG9hZHMvdXNlcnMvNDQ5L3Bvc3RzL18lZF80MGM3ZTNiZDRiODI3MjMxNTI1MjAzMjUyZTg2MDMwNS5qcGciO30=";
		String tryEncoding = NewsfeedUtils.encodeTypeContents(typeContents);
		
		assertEquals(encoded, tryEncoding);
	}

	@Test
	public void testDecodeTypeContents() {
		String[] typeContents = new String[] {
				"uploads/users/449/posts/_%d_65237fe755c10fff98eed3c9437a5b38.jpg",
				"uploads/users/449/posts/_%d_40c7e3bd4b827231525203252e860305.jpg"
		};
		String encoded = "YToyOntpOjA7czo2NDoidXBsb2Fkcy91c2Vycy80NDkvcG9zdHMvXyVkXzY1MjM3ZmU3NTVjMTBmZmY5OGVlZDNjOTQzN2E1YjM4LmpwZyI7aToxO3M6NjQ6InVwbG9hZHMvdXNlcnMvNDQ5L3Bvc3RzL18lZF80MGM3ZTNiZDRiODI3MjMxNTI1MjAzMjUyZTg2MDMwNS5qcGciO30=";
		List<String> tryDecoding = NewsfeedUtils.decodeTypeContents(encoded, null, null);
		
		assertArrayEquals(typeContents, tryDecoding.toArray());
	}

	@Test
	public void testEncodeLink() throws FailingHttpStatusCodeException, MalformedURLException, IOException, WebCrawlingFailedException {
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);

		String uri = "https://www.youtube.com/watch?v=cKQ2__zur7Y";
		String encoded = "YTo0OntzOjQ6ImxpbmsiO3M6NDI6Imh0dHA6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1jS1EyX196dXI3WSI7czo1OiJ0aXRsZSI7czo0OToiTmllciAtIEFzaGVzIE9mIERyZWFtcyAtIExpdmUgKEhEKSA3MjBwIC0gWW91VHViZSI7czoxMToiZGVzY3JpcHRpb24iO3M6OTg6IkFsbCBSaWdodHMgYmVsb25ncyB0byBTcXVhcmUgRW5peCAmcXVvdDtQS2dhbWVyIE5pZVIgTXVzaWMgQ29uY2VydCBUYWxrIExpdmUgQXNoZXMgT2YgRHJlYW1zJnF1b3Q7IjtzOjU6ImltYWdlIjtzOjA6IiI7fQ==";
		
		@SuppressWarnings("resource")
		WebClient client = new WebClient(BrowserVersion.INTERNET_EXPLORER);
		client.getOptions().setUseInsecureSSL(true);
		client.getOptions().setThrowExceptionOnScriptError(false);
		client.getOptions().setThrowExceptionOnFailingStatusCode(false);
		
		HtmlPage page = client.getPage(uri);
		HtmlMeta desc = page.getFirstByXPath("//meta[@property='og:description']");
		HtmlMeta img = page.getFirstByXPath("//meta[@property='og:image']");
		
		Map<String, String> link = new LinkedHashMap<String, String>();
		link.put("link", uri);
		link.put("title", page.getTitleText());
		link.put("description", desc.getContentAttribute());
		link.put("image", img.getContentAttribute());
		encoded = new String(DatatypeConverter.printBase64Binary(Pherialize.serialize(link).getBytes()));
		String tryEncoding = NewsfeedUtils.encodeLink(uri);
		
		assertEquals(encoded, tryEncoding);
	}

	@Test
	public void testDecodeLink() {
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		String encoded = "YTo0OntzOjQ6ImxpbmsiO3M6NDI6Imh0dHA6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1jS1EyX196dXI3WSI7czo1OiJ0aXRsZSI7czo0OToiTmllciAtIEFzaGVzIE9mIERyZWFtcyAtIExpdmUgKEhEKSA3MjBwIC0gWW91VHViZSI7czoxMToiZGVzY3JpcHRpb24iO3M6OTg6IkFsbCBSaWdodHMgYmVsb25ncyB0byBTcXVhcmUgRW5peCAmcXVvdDtQS2dhbWVyIE5pZVIgTXVzaWMgQ29uY2VydCBUYWxrIExpdmUgQXNoZXMgT2YgRHJlYW1zJnF1b3Q7IjtzOjU6ImltYWdlIjtzOjA6IiI7fQ==";
		Link link = new Link();
		link.setLink("https://www.youtube.com/watch?v=cKQ2__zur7Y");
		link.setTitle("Nier - Ashes Of Dreams - Live (HD) 720p - YouTube");
		link.setDescription("All Rights belongs to Square Enix \"PKgamer NieR Music Concert Talk Live Ashes Of Dreams\"");
		link.setImage("https://i.ytimg.com/vi/cKQ2__zur7Y/hqdefault.jpg");
		encoded = "YTo0OntzOjQ6ImxpbmsiO3M6NDM6Imh0dHBzOi8vd3d3LnlvdXR1YmUuY29tL3dhdGNoP3Y9Y0tRMl9fenVyN1kiO3M6NToidGl0bGUiO3M6NDk6Ik5pZXIgLSBBc2hlcyBPZiBEcmVhbXMgLSBMaXZlIChIRCkgNzIwcCAtIFlvdVR1YmUiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjg4OiJBbGwgUmlnaHRzIGJlbG9uZ3MgdG8gU3F1YXJlIEVuaXggIlBLZ2FtZXIgTmllUiBNdXNpYyBDb25jZXJ0IFRhbGsgTGl2ZSBBc2hlcyBPZiBEcmVhbXMiIjtzOjU6ImltYWdlIjtzOjQ4OiJodHRwczovL2kueXRpbWcuY29tL3ZpL2NLUTJfX3p1cjdZL2hxZGVmYXVsdC5qcGciO30=";
		Link tryDecoding = NewsfeedUtils.decodeLink(encoded);

		assertEquals(link.getLink(), tryDecoding.getLink());
		assertEquals(link.getTitle(), tryDecoding.getTitle());
		assertEquals(link.getDescription(), tryDecoding.getDescription());
		assertEquals(link.getImage(), tryDecoding.getImage());
	}

	@Test
	public void testEncodeTags() {
		String[] tags = new String[] {
				"16219",
				"343"
		};
		String encoded = "YToyOntpOjA7czo1OiIxNjIxOSI7aToxO3M6MzoiMzQzIjt9";
		String tryEncoding = NewsfeedUtils.encodeTags(tags);
		
		assertEquals(encoded, tryEncoding);
	}

	@Test
	public void testDecodeTags() {
		String[] tags = new String[] {
				"16219",
				"343"
		};
		String encoded = "YToyOntpOjA7czo1OiIxNjIxOSI7aToxO3M6MzoiMzQzIjt9";
		List<String> tryDecoding = NewsfeedUtils.decodeTags(encoded);
		
		assertArrayEquals(tags, tryDecoding.toArray());
	}

}
