package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.Like;
import com.diarium.collaborate.model.entity.Page;
import com.diarium.collaborate.model.response.LikeResponse;
import com.diarium.collaborate.model.response.PageInfoResponse;
import com.diarium.collaborate.model.response.PageResponse;
import com.diarium.collaborate.response.MultipleDataWithoutCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.LikeService;
import com.diarium.collaborate.service.PageService;

@RestController
@RequestMapping(value = "/mobile/page")
@PreAuthorize("hasAuthority('USER')")
public class PageController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PageController.class);

	@Autowired
	private PageService pageService;
	@Autowired
	private LikeService likeService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getPagesByUser(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get pages for user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Page> pages = pageService.getByUser(userId, limit, offset);
			List<PageInfoResponse> responses = new ArrayList<PageInfoResponse>();
			
			if (pages.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Page page : pages) {
				responses.add(pageService.getPageInfoResponse(page));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<PageInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/liker/{pageId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getLikers(@PathVariable Integer pageId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get likers for page with id: " + pageId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Like> likes = likeService.getByType("page", pageId, limit, offset);
			List<LikeResponse> responses = new ArrayList<LikeResponse>();
			
			if (likes.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Like like : likes) {
				responses.add(likeService.getLikeResponse(like));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<LikeResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/view/{id}")
	public ResponseEntity<ResponseTemplate> getPageById(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Get page with id: " + id);
		try {
			Page community = pageService.getById(id);
			
			if (community == null) {
				throw new DataDoesNotExistException();
			}
			
			PageResponse response = pageService.getPageResponse(community);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<PageResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
