package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataAlreadyExistException;
import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.entity.Emoticon;
import com.diarium.collaborate.model.entity.Newsfeed;
import com.diarium.collaborate.model.request.CreateBirthdayCardRequest;
import com.diarium.collaborate.model.request.CreateNewsfeedRequest;
import com.diarium.collaborate.model.request.EditNewsfeedRequest;
import com.diarium.collaborate.model.request.ShareNewsfeedRequest;
import com.diarium.collaborate.model.response.NewsfeedResponse;
import com.diarium.collaborate.response.MultipleDataWithoutCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.BirthdayCardService;
import com.diarium.collaborate.service.ConnectionService;
import com.diarium.collaborate.service.EmoticonService;
import com.diarium.collaborate.service.NewsfeedService;
import com.diarium.collaborate.utils.SecurityUtils;

@RestController
@RequestMapping(value = "/mobile/newsfeed")
@PreAuthorize("hasAuthority('USER')")
public class NewsfeedController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NewsfeedController.class);

	@Autowired
	private NewsfeedService newsfeedService;
	@Autowired
	private ConnectionService connectionService;
	@Autowired
	private BirthdayCardService birthdayCardService;
	@Autowired
	private EmoticonService emoticonService;

	@RequestMapping(method = RequestMethod.GET, value = "/dashboard/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getNewsfeedsForDashboard(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get newsfeeds for dasboard owned by user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Newsfeed> newsfeeds = newsfeedService.getForDashboard(userId, limit, offset);
			List<NewsfeedResponse> responses = new ArrayList<NewsfeedResponse>();
			
			if (newsfeeds.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Newsfeed newsfeed : newsfeeds) {
				responses.add(newsfeedService.getNewsfeedResponse(newsfeed, userId));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<NewsfeedResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/timeline/{ownerId}/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getNewsfeedsForTimeline(@PathVariable Integer ownerId, @PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get newsfeeds for timeline owned by user with id: " + ownerId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Newsfeed> newsfeeds = null;
			List<NewsfeedResponse> responses = new ArrayList<NewsfeedResponse>();
			
			if (userId == ownerId || ownerId == 0) {
				newsfeeds = newsfeedService.getForMyTimeline(userId, limit, offset);
			} else if (connectionService.isAlreadyFriends(userId, ownerId)) {
				newsfeeds = newsfeedService.getForFriendsTimeline(ownerId, userId, limit, offset);
			} else if (connectionService.isAlreadyFollower(userId, ownerId)) {
				newsfeeds = newsfeedService.getForFollowingsTimeline(ownerId, userId, limit, offset);
			} else {
				newsfeeds = newsfeedService.getForOthersTimeline(ownerId, userId, limit, offset);
			}
			
			if (newsfeeds.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Newsfeed newsfeed : newsfeeds) {
				responses.add(newsfeedService.getNewsfeedResponse(newsfeed, userId));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<NewsfeedResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageId}/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getNewsfeedsForPage(@PathVariable Integer pageId, @PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get newsfeeds for page with id: " + pageId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Newsfeed> newsfeeds = newsfeedService.getForPage(pageId, userId, limit, offset);
			List<NewsfeedResponse> responses = new ArrayList<NewsfeedResponse>();
			
			if (newsfeeds.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Newsfeed newsfeed : newsfeeds) {
				responses.add(newsfeedService.getNewsfeedResponse(newsfeed, userId));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<NewsfeedResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/community/{communityId}/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getNewsfeedsForCommunity(@PathVariable Integer communityId, @PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get newsfeeds for community with id: " + communityId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Newsfeed> newsfeeds = newsfeedService.getForCommunity(communityId, userId, limit, offset);
			List<NewsfeedResponse> responses = new ArrayList<NewsfeedResponse>();
			
			if (newsfeeds.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Newsfeed newsfeed : newsfeeds) {
				responses.add(newsfeedService.getNewsfeedResponse(newsfeed, userId));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<NewsfeedResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/view/{id}/{userId}")
	public ResponseEntity<ResponseTemplate> getNewsfeedById(@PathVariable Integer id, @PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Get newsfeed with id: " + id);
		try {
			Newsfeed newsfeed = newsfeedService.getById(id);
			
			if (newsfeed == null) {
				throw new DataDoesNotExistException();
			}
			
			NewsfeedResponse response = newsfeedService.getNewsfeedResponse(newsfeed, userId);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<NewsfeedResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<ResponseTemplate> createNewsfeed(@RequestBody CreateNewsfeedRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create newsfeed by user with id: " + request.getUserId());
		try {
			if (request.getUserId() == 0) {
				request.setUserId(SecurityUtils.getCollaborateUserIdFromPrincipal());
			}

			NewsfeedResponse response = newsfeedService.create(request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<NewsfeedResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (WebCrawlingFailedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/create/birthdayCard")
	public ResponseEntity<ResponseTemplate> createBirthdayCard(@RequestBody CreateBirthdayCardRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create newsfeed by user with id: " + request.getUserId());
		try {
			if (birthdayCardService.getOne(request.getUserId(), request.getToUserId()) != null) {
				throw new DataAlreadyExistException();
			}
			
			NewsfeedResponse response = birthdayCardService.create(request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<NewsfeedResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (WebCrawlingFailedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (DataAlreadyExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/edit")
	public ResponseEntity<ResponseTemplate> editNewsfeed(@RequestBody EditNewsfeedRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Edit newsfeed with id: " + request.getId());
		try {
			Newsfeed originalNewsfeed = newsfeedService.getById(request.getId());
			
			if (originalNewsfeed == null) {
				throw new DataDoesNotExistException();
			}
			
			NewsfeedResponse response = newsfeedService.edit(originalNewsfeed, request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<NewsfeedResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (WebCrawlingFailedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/delete/{id}")
	public ResponseEntity<ResponseTemplate> deleteNewsfeed(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Delete newsfeed with id: " + id);
		try {
			Newsfeed newsfeed = newsfeedService.getById(id);
			
			if (newsfeed == null) {
				throw new DataDoesNotExistException();
			}
			
			newsfeedService.delete(newsfeed);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (FileDeletionFailedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/share")
	public ResponseEntity<ResponseTemplate> shareNewsfeed(@RequestBody ShareNewsfeedRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Share newsfeed by user with id: " + request.getUserId());
		try {
			Newsfeed originalNewsfeed = newsfeedService.getById(request.getSharedId());
			
			if (originalNewsfeed == null) {
				throw new DataDoesNotExistException();
			}
			
			NewsfeedResponse response = newsfeedService.share(originalNewsfeed, request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<NewsfeedResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/emoticons")
	public ResponseEntity<ResponseTemplate> getEmoticons() {
		Long time = System.nanoTime();
		LOGGER.info("Get emoticons");
		try {
			List<Emoticon> emoticons = emoticonService.getAllEmoticons();
			
			if (emoticons.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<Emoticon>("200", "OK", emoticons, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
