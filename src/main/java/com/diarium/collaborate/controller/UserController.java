package com.diarium.collaborate.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.User;
import com.diarium.collaborate.model.response.UserInfoResponse;
import com.diarium.collaborate.model.response.UserProfileResponse;
import com.diarium.collaborate.response.MultipleDataWithoutCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.UserService;

@RestController
@RequestMapping(value = "/mobile/user")
@PreAuthorize("hasAuthority('USER')")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/profile/{userId}")
	public ResponseEntity<ResponseTemplate> getProfile(@PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Get profile for user with id: " + userId);
		try {
			User user = userService.getById(userId);
			
			if (user == null) {
				throw new DataDoesNotExistException();
			}
			
			UserProfileResponse response = userService.getUserProfileResponse(user);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<UserProfileResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/search/{name}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getByName(@PathVariable String name, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get by name: " + name);
		try {
			List<User> users = userService.getByName("%" + name + "%", limit, offset);
			List<UserInfoResponse> responses = new ArrayList<UserInfoResponse>();
			
			if (users.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (User user : users) {
				LOGGER.info("Get user with id: " + user.getId() + ", name: " + user.getFullname());
				responses.add(userService.getUserInfoResponse(user));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<UserInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/search/friend/{userId}/{friendName}")
	public ResponseEntity<ResponseTemplate> getFriendsByName(@PathVariable Integer userId, @PathVariable String friendName) {
		Long time = System.nanoTime();
		LOGGER.info("Get friends of user with id: " + userId + ", by name: " + friendName);
		try {
			List<User> users = userService.getFriendsByName(userId, "%" + friendName + "%");
			List<UserInfoResponse> responses = new ArrayList<UserInfoResponse>();
			
			if (users.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (User user : users) {
				LOGGER.info("Get user with id: " + user.getId() + ", name: " + user.getFullname());
				responses.add(userService.getUserInfoResponse(user));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<UserInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/search/following/{userId}/{friendName}")
	public ResponseEntity<ResponseTemplate> getFollowingsByName(@PathVariable Integer userId, @PathVariable String friendName) {
		Long time = System.nanoTime();
		LOGGER.info("Get followings of user with id: " + userId + ", by name: " + friendName);
		try {
			List<User> users = userService.getFollowingsByName(userId, "%" + friendName + "%");
			List<UserInfoResponse> responses = new ArrayList<UserInfoResponse>();
			
			if (users.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (User user : users) {
				LOGGER.info("Get user with id: " + user.getId() + ", name: " + user.getFullname());
				responses.add(userService.getUserInfoResponse(user));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<UserInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/search/follower/{userId}/{friendName}")
	public ResponseEntity<ResponseTemplate> getFollowersByName(@PathVariable Integer userId, @PathVariable String friendName) {
		Long time = System.nanoTime();
		LOGGER.info("Get followers of user with id: " + userId + ", by name: " + friendName);
		try {
			List<User> users = userService.getFollowersByName(userId, "%" + friendName + "%");
			List<UserInfoResponse> responses = new ArrayList<UserInfoResponse>();
			
			if (users.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (User user : users) {
				LOGGER.info("Get user with id: " + user.getId() + ", name: " + user.getFullname());
				responses.add(userService.getUserInfoResponse(user));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<UserInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/birthday/friend/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getFriendsWhoHaveBirthdayToday(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get friends of user with id: " + userId + ", that have their birthday on: " + LocalDate.now().toString() + ", limit: " + limit + ", offset: " + offset);
		try {
			List<User> users = userService.getFriendsWhoHaveBirthdayToday(userId, limit, offset);
			List<UserInfoResponse> responses = new ArrayList<UserInfoResponse>();
			
			if (users.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (User user : users) {
				LOGGER.info("Get user with id: " + user.getId() + ", name: " + user.getFullname());
				responses.add(userService.getUserInfoResponse(user));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<UserInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
