package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataAlreadyExistException;
import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.Expression;
import com.diarium.collaborate.model.entity.Like;
import com.diarium.collaborate.model.request.EditExpressionRequest;
import com.diarium.collaborate.model.request.GiveExpressionRequest;
import com.diarium.collaborate.model.response.LikeResponse;
import com.diarium.collaborate.response.MultipleDataWithoutCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.ExpressionService;
import com.diarium.collaborate.service.LikeService;

@RestController
@RequestMapping(value = "/mobile/like")
@PreAuthorize("hasAuthority('USER')")
public class LikeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LikeController.class);

	@Autowired
	private LikeService likeService;
	@Autowired
	private ExpressionService expressionService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/getbytype/{type}/{typeId}/{emoji}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getByTypeAndTypeId(@PathVariable String type, @PathVariable Integer typeId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get likes for " + type + "  with id: " + typeId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Like> likes = likeService.getByType(type, typeId, limit, offset);
			List<LikeResponse> responses = new ArrayList<LikeResponse>();
			
			if (likes.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Like like : likes) {
				responses.add(likeService.getLikeResponse(like));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<LikeResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/express")
	public ResponseEntity<ResponseTemplate> giveExpression(@RequestBody GiveExpressionRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create like by user with id: " + request.getUserId());
		try {
			Like like = likeService.getByUserAndType(request.getUserId(), request.getType(), request.getTypeId());
			
			if (like != null) {
				throw new DataAlreadyExistException();
			}
			
			LikeResponse response = likeService.create(request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<LikeResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataAlreadyExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/express/edit")
	public ResponseEntity<ResponseTemplate> edit(@RequestBody EditExpressionRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Edit like for " + request.getType() + " with id: " + request.getTypeId() + ", from user with id: " + request.getUserId());
		try {
			Like originalLike = likeService.getByUserAndType(request.getUserId(), request.getType(), request.getTypeId());
			
			if (originalLike == null) {
				throw new DataDoesNotExistException();
			}
			
			LikeResponse response = likeService.edit(originalLike, request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<LikeResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/unlike/{userId}/{type}/{typeId}")
	public ResponseEntity<ResponseTemplate> unlike(@PathVariable Integer userId, @PathVariable String type, @PathVariable Integer typeId) {
		Long time = System.nanoTime();
		LOGGER.info("Delete like for " + type + " with id: " + typeId + ", from user with id: " + userId);
		try {
			Like like = likeService.getByUserAndType(userId, type, typeId);
			
			if (like == null) {
				throw new DataDoesNotExistException();
			}
			
			likeService.delete(like);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/expressions")
	public ResponseEntity<ResponseTemplate> getExpressions() {
		Long time = System.nanoTime();
		LOGGER.info("Get expressions");
		try {
			List<Expression> expressions = expressionService.getAllExpressions();
			
			if (expressions.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<Expression>("200", "OK", expressions, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
