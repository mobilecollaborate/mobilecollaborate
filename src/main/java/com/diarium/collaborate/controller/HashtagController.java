package com.diarium.collaborate.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.Hashtag;
import com.diarium.collaborate.response.MultipleDataWithCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.HashtagService;

@RestController
@RequestMapping(value = "/mobile/hashtag")
@PreAuthorize("hasAuthority('USER')")
public class HashtagController {

	private static final Logger LOGGER = LoggerFactory.getLogger(HashtagController.class);
	
	@Autowired
	private HashtagService hashtagService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/topten")
	public ResponseEntity<ResponseTemplate> getTopTenHashtags() {
		Long time = System.nanoTime();
		LOGGER.info("Get top ten hashtags");
		try {
			List<Hashtag> hashtags = hashtagService.getTopTen();
			
			if (hashtags.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<Hashtag>("200", "OK", hashtags, 10, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/add/{hash}")
	public ResponseEntity<ResponseTemplate> addHashtag(@PathVariable String hash) {
		Long time = System.nanoTime();
		LOGGER.info("Add hashtag");
		try {
			Hashtag hashtag = hashtagService.add(hash);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<Hashtag>("200", "OK", hashtag, time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
