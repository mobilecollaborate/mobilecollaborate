package com.diarium.collaborate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.model.response.GreetingResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.GreetingService;

@RestController
@RequestMapping(value = "/mobile/greeting")
@PreAuthorize("hasAuthority('USER')")
public class GreetingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NewsfeedController.class);
	
	@Autowired
	private GreetingService greetingService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{name}/{region}/{city}")
	public ResponseEntity<ResponseTemplate> getGreeting(@PathVariable String name, @PathVariable String region, @PathVariable String city) {
		Long time = System.nanoTime();
		LOGGER.info("Get greeting for: " + name);
		try {
			GreetingResponse response = greetingService.getGreetingResponse(name, region + "/" + city);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<GreetingResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
