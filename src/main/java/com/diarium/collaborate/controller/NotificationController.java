package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataAlreadyExistException;
import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.Notification;
import com.diarium.collaborate.model.entity.NotificationReceiver;
import com.diarium.collaborate.model.response.NotificationResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.MultipleDataWithCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.service.NotificationReceiverService;
import com.diarium.collaborate.service.NotificationService;

@RestController
@RequestMapping(value = "/mobile/notificaion")
@PreAuthorize("hasAuthority('USER')")
public class NotificationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

	@Autowired
	private NotificationService notificationService;
	@Autowired
	private NotificationReceiverService notificationReceiverService;

	@RequestMapping(method = RequestMethod.GET, value = "/{toUserId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getNotifications(@PathVariable Integer toUserId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get notifications for user with id: " + toUserId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Notification> notifications = notificationService.getByToUser(toUserId, limit, offset);
			List<NotificationResponse> responses = new ArrayList<NotificationResponse>();
			
			if (notifications.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Notification notification : notifications) {
				responses.add(notificationService.getNotificationResponse(notification));
			}
			
			Integer count = notificationService.countByUser(toUserId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<NotificationResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/delete/{id}")
	public ResponseEntity<ResponseTemplate> deleteNotification(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Delete notification with id: " + id);
		try {
			Notification newsfeed = notificationService.getById(id);
			
			if (newsfeed == null) {
				throw new DataDoesNotExistException();
			}
			
			notificationService.delete(newsfeed);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/turnon/{userId}/{type}/{typeId}")
	public ResponseEntity<ResponseTemplate> turnOnNotification(@PathVariable Integer userId, @PathVariable String type, @PathVariable Integer typeId) {
		Long time = System.nanoTime();
		LOGGER.info("Turn on notification for " + type + "  with id: " + typeId + " to user with id: " + userId);
		try {
			NotificationReceiver notificationReceiver = notificationReceiverService.getByUserAndType(userId, type, typeId);
			
			if (notificationReceiver != null) {
				throw new DataAlreadyExistException();
			}
			
			notificationReceiverService.create(userId, type, typeId);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataAlreadyExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/turnoff/{userId}/{type}/{typeId}")
	public ResponseEntity<ResponseTemplate> turnOffNotification(@PathVariable Integer userId, @PathVariable String type, @PathVariable Integer typeId) {
		Long time = System.nanoTime();
		LOGGER.info("Turn off notification for " + type + "  with id: " + typeId + " to user with id: " + userId);
		try {
			NotificationReceiver notificationReceiver = notificationReceiverService.getByUserAndType(userId, type, typeId);
			
			if (notificationReceiver == null) {
				throw new DataDoesNotExistException();
			}
			
			notificationReceiverService.delete(notificationReceiver);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
