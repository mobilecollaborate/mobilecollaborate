package com.diarium.collaborate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.diarium.collaborate.exception.EmptyFileUploadedException;
import com.diarium.collaborate.exception.UnsupportedFileExtensionException;
import com.diarium.collaborate.exception.UnsupportedUploadTypeException;
import com.diarium.collaborate.model.response.FilenameResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.utils.FileUtils;

import io.minio.MinioClient;

@RestController
@RequestMapping(value = "/mobile/file")
@PreAuthorize("hasAuthority('USER')")
public class FileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileController.class);
	
	@SuppressWarnings("resource")
	@RequestMapping(method = RequestMethod.POST, value = "/upload/single/{uploadType}/{userId}")
	public ResponseEntity<ResponseTemplate> handleSingleFileUpload(@RequestParam("file") MultipartFile multipartFile, @PathVariable String uploadType, @PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Uploading with filename: " + multipartFile.getOriginalFilename() + ", type: " + uploadType);
		try {
			if (multipartFile.isEmpty()) {
				throw new EmptyFileUploadedException();
			}
			
			String uploadPath = new String();
			String filename = FileUtils.encodeFilename(userId + multipartFile.getOriginalFilename() + System.currentTimeMillis());
			String[] strings = multipartFile.getOriginalFilename().split("\\.");
			String extension = strings[strings.length - 1].toLowerCase();
			FilenameResponse response = new FilenameResponse();
			
			switch (uploadType) {
			case "avatar":
				if (!FileUtils.isImage(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/users/" + userId + "/";
				break;
				
			case "logo-page":
				if (!FileUtils.isImage(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/logos-page/" + userId + "/";
				break;
				
			case "logo-community":
				if (!FileUtils.isImage(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/logos-community/" + userId + "/";
				break;
				
			case "cover":
				if (!FileUtils.isImage(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/users/" + userId + "/";
				break;
				
			case "newsfeed":
				if (!FileUtils.isImage(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/users/" + userId + "/posts/";
				break;
				
			case "comment":
				if (!FileUtils.isImage(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/users/" + userId + "/comments/";
				break;
				
			case "video":
				if (!FileUtils.isVideo(extension)) {
					throw new UnsupportedFileExtensionException();
				}
				
				uploadPath += "uploads/videos/" + userId + "/";
				break;
				
			case "file":
				uploadPath += "uploads/files/" + userId + "/";
				break;
				
			default:
				throw new UnsupportedUploadTypeException();
			}
			
			MinioClient minioClient = FileUtils.getMinioClient();
			
			if (FileUtils.isImage(extension)) {
				String savePath = uploadPath + "_50_" + filename + "." + extension;
				minioClient.putObject(FileUtils.getMinioBucketName(), savePath, multipartFile.getInputStream(), multipartFile.getContentType());
				savePath = uploadPath + "_200_" + filename + "." + extension;
				minioClient.putObject(FileUtils.getMinioBucketName(), savePath, multipartFile.getInputStream(), multipartFile.getContentType());
				savePath = uploadPath + "_600_" + filename + "." + extension;
				minioClient.putObject(FileUtils.getMinioBucketName(), savePath, multipartFile.getInputStream(), multipartFile.getContentType());
				savePath = uploadPath + "_960_" + filename + "." + extension;
				minioClient.putObject(FileUtils.getMinioBucketName(), savePath, multipartFile.getInputStream(), multipartFile.getContentType());
				uploadPath += "_%d_" + filename + "." + extension;
			} else {
				uploadPath += filename + "." + extension;
				minioClient.putObject(FileUtils.getMinioBucketName(), uploadPath, multipartFile.getInputStream(), multipartFile.getContentType());
			}
			
			response.setPath(uploadPath);
			response.setFilename(multipartFile.getOriginalFilename());
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<String>("200", "OK", uploadPath, time), HttpStatus.OK);
		} catch (EmptyFileUploadedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (UnsupportedFileExtensionException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (UnsupportedUploadTypeException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		} finally {
//			outputStream.close();
		}
	}
}
