package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.Community;
import com.diarium.collaborate.model.entity.CommunityMember;
import com.diarium.collaborate.model.response.CommunityInfoResponse;
import com.diarium.collaborate.model.response.CommunityMemberResponse;
import com.diarium.collaborate.model.response.CommunityResponse;
import com.diarium.collaborate.response.MultipleDataWithoutCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.service.CommunityMemberService;
import com.diarium.collaborate.service.CommunityService;

@RestController
@RequestMapping(value = "/mobile/community")
@PreAuthorize("hasAuthority('USER')")
public class CommunityController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommunityController.class);

	@Autowired
	private CommunityService communityService;
	@Autowired
	private CommunityMemberService communityMemberService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getCommunitiesByUser(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get communities for user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Community> communities = communityService.getByUser(userId, limit, offset);
			List<CommunityInfoResponse> responses = new ArrayList<CommunityInfoResponse>();
			
			if (communities.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Community community : communities) {
				responses.add(communityService.getCommunityInfoResponse(community));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<CommunityInfoResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/member/{communityId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getCommunityMembers(@PathVariable Integer communityId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get members for community with id: " + communityId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<CommunityMember> members = communityMemberService.getByCommunity(communityId);
			List<CommunityMemberResponse> responses = new ArrayList<CommunityMemberResponse>();
			
			if (members.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (CommunityMember member : members) {
				responses.add(communityMemberService.getCommunityMemberResponse(member));
			}
			
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithoutCountResponse<CommunityMemberResponse>("200", "OK", responses, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/view/{id}")
	public ResponseEntity<ResponseTemplate> getCommunityById(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Get community with id: " + id);
		try {
			Community community = communityService.getById(id);
			
			if (community == null) {
				throw new DataDoesNotExistException();
			}
			
			CommunityResponse response = communityService.getCommunityResponse(community);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<CommunityResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
