package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.model.entity.Comment;
import com.diarium.collaborate.model.request.AddCommentRequest;
import com.diarium.collaborate.model.request.EditCommentRequest;
import com.diarium.collaborate.model.response.CommentResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.MultipleDataWithCountResponse;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.service.CommentService;

@RestController
@RequestMapping(value = "/mobile/comment")
@PreAuthorize("hasAuthority('USER')")
public class CommentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommentController.class);
	
	@Autowired
	private CommentService commentService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/getbytype/{type}/{typeId}/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getCommentsByType(@PathVariable String type, @PathVariable Integer typeId, @PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get comments for " + type + "  with id: " + typeId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Comment> comments = commentService.getByType(type, typeId, limit, offset);
			List<CommentResponse> responses = new ArrayList<CommentResponse>();
			
			if (comments.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Comment comment : comments) {
				responses.add(commentService.getCommentResponse(comment, userId));
			}
			
			Integer count = commentService.countByType(type, typeId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<CommentResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/addcomment")
	public ResponseEntity<ResponseTemplate> addComment(@RequestBody AddCommentRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create comment by user with id: " + request.getUserId());
		try {
			CommentResponse response = commentService.create(request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<CommentResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/edit")
	public ResponseEntity<ResponseTemplate> editComment(@RequestBody EditCommentRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Edit comment with id: " + request.getId());
		try {
			Comment originalComment = commentService.getById(request.getId());
			
			if (originalComment == null) {
				throw new DataDoesNotExistException();
			}
			
			CommentResponse response = commentService.edit(originalComment, request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<CommentResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/delete/{id}")
	public ResponseEntity<ResponseTemplate> deleteComment(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Delete comment with id: " + id);
		try {
			Comment comment = commentService.getById(id);
			
			if (comment == null) {
				throw new DataDoesNotExistException();
			}
			
			commentService.delete(comment);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (FileDeletionFailedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
