package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataAlreadyExistException;
import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.model.entity.Connection;
import com.diarium.collaborate.model.request.CreateConnectionRequest;
import com.diarium.collaborate.model.response.FriendRequestResponse;
import com.diarium.collaborate.model.response.ConnectionResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.MultipleDataWithCountResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.service.ConnectionService;

@RestController
@RequestMapping(path="/mobile/connection")
@PreAuthorize("hasAuthority('USER')")
public class ConnectionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionController.class);
	
	@Autowired
	private ConnectionService connectionService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/friends/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getFriends(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get friends for user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Connection> connections = connectionService.getFriends(userId, limit, offset);
			List<ConnectionResponse> responses = new ArrayList<ConnectionResponse>();
			
			if (connections.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Connection connection : connections) {
				responses.add(connectionService.getConnectionResponse(connection, userId));
			}
			
			Integer count = connectionService.countFriends(userId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<ConnectionResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/followings/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getFollowings(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get followings for user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Connection> connections = connectionService.getFollowings(userId, limit, offset);
			List<ConnectionResponse> responses = new ArrayList<ConnectionResponse>();
			
			if (connections.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Connection connection : connections) {
				responses.add(connectionService.getConnectionResponse(connection, userId));
			}
			
			Integer count = connectionService.countFollowings(userId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<ConnectionResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/followers/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getFollowers(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get followers for user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Connection> connections = connectionService.getFollowers(userId, limit, offset);
			List<ConnectionResponse> responses = new ArrayList<ConnectionResponse>();
			
			if (connections.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Connection connection : connections) {
				responses.add(connectionService.getConnectionResponse(connection, userId));
			}
			
			Integer count = connectionService.countFollowers(userId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<ConnectionResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/friendrequests/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getFriendRequests(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get friend requests for user with id: " + userId + ", limit: " + limit + ", offset: " + offset);
		try {
			List<Connection> connections = connectionService.getFriendRequests(userId, limit, offset);
			List<FriendRequestResponse> responses = new ArrayList<FriendRequestResponse>();
			
			if (connections.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Connection connection : connections) {
				responses.add(connectionService.getFriendRequestResponse(connection));
			}
			
			Integer count = connectionService.countFriendRequests(userId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<FriendRequestResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/addfriend")
	public ResponseEntity<ResponseTemplate> addFriend(@RequestBody CreateConnectionRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create friend connections by user with id: " + request.getUserId() + " to: " + request.getToUserId());
		try {
			if (connectionService.isAlreadyFriends(request.getUserId(), request.getToUserId())) {
				throw new DataAlreadyExistException();
			} else {
				connectionService.create(request, 2, 0);
			}

			if (!connectionService.isAlreadyFollower(request.getUserId(), request.getToUserId())) {
				connectionService.create(request, 1, 1);
			}
			
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataAlreadyExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/follow")
	public ResponseEntity<ResponseTemplate> follow(@RequestBody CreateConnectionRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create follower connections by user with id: " + request.getUserId() + " to: " + request.getToUserId());
		try {
			if (connectionService.isAlreadyFollower(request.getUserId(), request.getToUserId())) {
				throw new DataAlreadyExistException();
			}
			
			connectionService.create(request, 1, 1);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/confirm/{toUserId}/{userId}")
	public ResponseEntity<ResponseTemplate> confirm(@PathVariable Integer toUserId, @PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Confirm friend request from user with id: " + userId + " to: " + toUserId);
		try {
			Connection connection = connectionService.getFriendRequestConnection(userId, toUserId);
			
			if (connection == null) {
				throw new DataDoesNotExistException();
			}
			
			connectionService.edit(connection, 2, 1);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/reject/{toUserId}/{userId}")
	public ResponseEntity<ResponseTemplate> reject(@PathVariable Integer toUserId, @PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Reject friend request from user with id: " + userId + " to: " + toUserId);
		try {
			Connection connection = connectionService.getFriendRequestConnection(userId, toUserId);
			
			if (connection == null) {
				throw new DataDoesNotExistException();
			}
			
			connectionService.delete(connection);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/unfriend/{toUserId}/{userId}")
	public ResponseEntity<ResponseTemplate> unfriend(@PathVariable Integer toUserId, @PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Delete friend connections from user with id: " + userId + " to: " + toUserId);
		try {
			List<Connection> connections = connectionService.getFriendConnections(userId, toUserId);
			
			if (connections.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Connection connection : connections) {
				if (connection != null) {
					connectionService.delete(connection);
				}
			}
			
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/unfollow/{toUserId}/{userId}")
	public ResponseEntity<ResponseTemplate> unfollow(@PathVariable Integer toUserId, @PathVariable Integer userId) {
		Long time = System.nanoTime();
		LOGGER.info("Delete following connection from user with id: " + userId + " to: " + toUserId);
		try {
			Connection connection = connectionService.getFollowingConnection(userId, toUserId);
			
			if (connection == null) {
				throw new DataDoesNotExistException();
			}
			
			connectionService.delete(connection);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}