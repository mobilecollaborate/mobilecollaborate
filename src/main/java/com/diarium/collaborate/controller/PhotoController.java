package com.diarium.collaborate.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.exception.DataDoesNotExistException;
import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.model.entity.Photo;
import com.diarium.collaborate.model.entity.PhotoAlbum;
import com.diarium.collaborate.model.request.CreatePhotoAlbumRequest;
import com.diarium.collaborate.model.response.PhotoAlbumResponse;
import com.diarium.collaborate.model.response.PhotoResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.response.MultipleDataWithCountResponse;
import com.diarium.collaborate.response.SingleDataResponse;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.service.PhotoAlbumService;
import com.diarium.collaborate.service.PhotoService;

@RestController
@RequestMapping(value = "/mobile/photo")
@PreAuthorize("hasAuthority('USER')")
public class PhotoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PhotoController.class);

	@Autowired
	private PhotoService photoService;
	@Autowired
	private PhotoAlbumService photoAlbumService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getPhotos(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get photos for user with id: " + userId);
		try {
			List<Photo> photos = photoService.getByUser(userId, limit, offset);
			List<PhotoResponse> responses = new ArrayList<PhotoResponse>();
			
			if (photos.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (Photo photo : photos) {
				responses.add(photoService.getPhotoResponse(photo));
			}
			
			Integer count = photoService.countByUser(userId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<PhotoResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/delete/{id}")
	public ResponseEntity<ResponseTemplate> deletePhoto(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Delete photo with id: " + id);
		try {
			Photo photo = photoService.getById(id);
			
			if (photo == null) {
				throw new DataDoesNotExistException();
			}
			
			photoService.delete(photo);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (FileDeletionFailedException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/album/{userId}/{limit}/{offset}")
	public ResponseEntity<ResponseTemplate> getPhotoAlbums(@PathVariable Integer userId, @PathVariable Integer limit, @PathVariable Integer offset) {
		Long time = System.nanoTime();
		LOGGER.info("Get photo albums for user with id: " + userId);
		try {
			List<PhotoAlbum> photoAlbums = photoAlbumService.getByUser(userId, limit, offset);
			List<PhotoAlbumResponse> responses = new ArrayList<PhotoAlbumResponse>();
			
			if (photoAlbums.isEmpty()) {
				throw new DataDoesNotExistException();
			}
			
			for (PhotoAlbum photoAlbum : photoAlbums) {
				responses.add(photoAlbumService.getPhotoAlbumResponse(photoAlbum));
			}
			
			Integer count = photoAlbumService.countByUser(userId);
			return new ResponseEntity<ResponseTemplate>(new MultipleDataWithCountResponse<PhotoAlbumResponse>("200", "OK", responses, count, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/album/view/{id}")
	public ResponseEntity<ResponseTemplate> getPhotoAlbumById(@PathVariable Integer id) {
		Long time = System.nanoTime();
		LOGGER.info("Get photo album with id: " + id);
		try {
			PhotoAlbum photoAlbum = photoAlbumService.getById(id);
			
			if (photoAlbum == null) {
				throw new DataDoesNotExistException();
			}
			
			PhotoAlbumResponse response = photoAlbumService.getPhotoAlbumResponse(photoAlbum);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<PhotoAlbumResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (DataDoesNotExistException e) {
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse(e.getResponseCode(), e.getMessage(), time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/album/create")
	public ResponseEntity<ResponseTemplate> createPhotoAlbum(@RequestBody CreatePhotoAlbumRequest request) {
		Long time = System.nanoTime();
		LOGGER.info("Create photo album by user with id: " + request.getUserId());
		try {
			PhotoAlbumResponse response = photoAlbumService.create(request);
			return new ResponseEntity<ResponseTemplate>(new SingleDataResponse<PhotoAlbumResponse>("200", "OK", response, time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
