package com.diarium.collaborate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.diarium.collaborate.model.entity.Report;
import com.diarium.collaborate.response.NoDataResponse;
import com.diarium.collaborate.response.ResponseTemplate;
import com.diarium.collaborate.service.ReportService;

@RestController
@RequestMapping(value = "/mobile/report")
@PreAuthorize("hasAuthority('USER')")
public class ReportController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private ReportService reportService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ResponseEntity<ResponseTemplate> createReport(@RequestBody Report report) {
		Long time = System.nanoTime();
		LOGGER.info("Create repost for " + report.getType() + " by user with id: " + report.getUserId());
		try {
			reportService.create(report);
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("200", "OK", time), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseTemplate>(new NoDataResponse("500", "Internal server error with message: " + e.getMessage(), time), HttpStatus.OK);
		}
	}
}
