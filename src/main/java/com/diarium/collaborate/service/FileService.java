package com.diarium.collaborate.service;

import com.diarium.collaborate.exception.FileDeletionFailedException;

public interface FileService {

	void deleteFile(String path, boolean isImage) throws FileDeletionFailedException;
}
