package com.diarium.collaborate.service;

import com.diarium.collaborate.model.entity.Report;

public interface ReportService {

	void create(Report report);
}
