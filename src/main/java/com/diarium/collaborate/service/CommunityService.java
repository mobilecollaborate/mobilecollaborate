package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Community;
import com.diarium.collaborate.model.response.CommunityInfoResponse;
import com.diarium.collaborate.model.response.CommunityResponse;

public interface CommunityService {

	List<Community> getByUser(Integer userId, Integer limit, Integer offset);
	Community getById(Integer id);
	
	CommunityInfoResponse getCommunityInfoResponse(Community community);
	CommunityResponse getCommunityResponse(Community community);
}
