package com.diarium.collaborate.service.implementation;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.User;
import com.diarium.collaborate.model.response.UserInfoResponse;
import com.diarium.collaborate.model.response.UserProfileResponse;
import com.diarium.collaborate.repository.UserRepository;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;
	
	@Override
	public List<User> getByName(String name, Integer limit, Integer offset) {
		return repository.findByName(name, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<User> getFriendsByName(Integer userId, String name) {
		return repository.findFriendsByName(userId, name);
	}
	
	@Override
	public List<User> getFollowingsByName(Integer userId, String name) {
		return repository.findFollowingsByName(userId, name);
	}
	
	@Override
	public List<User> getFollowersByName(Integer userId, String name) {
		return repository.findFollowersByName(userId, name);
	}
	
	@Override
	public List<User> getFriendsWhoHaveBirthdayToday(Integer userId, Integer limit, Integer offset) {
		LocalDate date = LocalDate.now();
		return repository.findFriendsWhoBirthdayToday(userId, date.getDayOfMonth(), date.getMonthValue(), LocalDate.now().getYear(), new Integer(offset*limit), limit);
	}
	
	@Override
	public User getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public UserInfoResponse getUserInfoResponse(User user) {
		if (user != null) {
			UserInfoResponse response = new UserInfoResponse();
			response.setId(user.getId());
			response.setFullname(user.getFullname());
			response.setUsername(user.getUsername());
			response.setAvatar(CommonUtils.getCollaborateBaseUrl() + "uploads/users/" + user.getId() + "/");
			response.setLastActiveTime(user.getLastActiveTime());
			response.setOnlineStatus(user.getOnlineStatus());
			response.setJabatan(user.getJabatan());
			response.setAge(LocalDate.now().getYear() - user.getBirthYear());
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public UserProfileResponse getUserProfileResponse(User user) {
		if (user != null) {
			UserProfileResponse response = new UserProfileResponse();
			response.setId(user.getId());
			response.setFullname(user.getFullname());
			response.setUsername(user.getUsername());
			response.setEmailAddress(user.getEmailAddress());
			response.setGenre(user.getGenre());
			response.setBio(user.getBio());
			
			if (user.getCover() != null) {
				if (!user.getCover().isEmpty()) {
					response.setCover(CommonUtils.getCollaborateBaseUrl() + user.getCover());
				}
			}
			
			response.setCountry(user.getCountry());
			response.setAvatar(CommonUtils.getCollaborateBaseUrl() + "uploads/users/" + user.getId() + "/");
			response.setLastActiveTime(user.getLastActiveTime());
			response.setCreatedAt(CommonUtils.getDuration(user.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(user.getUpdatedAt().getTime()));
			response.setCity(user.getCity());
			
			if (user.getOriginalCover() != null) {
				if (!user.getOriginalCover().isEmpty()) {
					response.setOriginalCover(CommonUtils.getCollaborateBaseUrl() + user.getOriginalCover());
				}
			}
			
			response.setOnlineStatus(user.getOnlineStatus());
			response.setBanned(user.getBanned());
			response.setBirthDay(user.getBirthDay());
			response.setBirthMonth(user.getBirthMonth());
			response.setBirthYear(user.getBirthYear());
			response.setLdap(user.getLdap());
			response.setJabatan(user.getJabatan());
			response.setBand(user.getBand());
			response.setKota(user.getKota());
			response.setLoker(user.getLoker());
			response.setDivisi(user.getDivisi());
			response.setUnit(user.getUnit());
			response.setReligion(user.getReligion());
			response.setMaritalStatus(user.getMaritalStatus());
			response.setEmployeeGroup(user.getEmployeeGroup());
			response.setEmployeeSubgroup(user.getEmployeeSubgroup());
			response.setEthnicGroup(user.getEthnicGroup());
			response.setBloodType(user.getBloodType());
			return response;
		} else {
			return null;
		}
	}
}
