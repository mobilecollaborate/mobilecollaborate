package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.entity.BirthdayCard;
import com.diarium.collaborate.model.entity.Comment;
import com.diarium.collaborate.model.entity.Like;
import com.diarium.collaborate.model.entity.Newsfeed;
import com.diarium.collaborate.model.entity.NotificationReceiver;
import com.diarium.collaborate.model.entity.Photo;
import com.diarium.collaborate.model.request.CreateNewsfeedRequest;
import com.diarium.collaborate.model.request.CreatePhotoAlbumRequest;
import com.diarium.collaborate.model.request.CreatePhotoRequest;
import com.diarium.collaborate.model.request.EditNewsfeedRequest;
import com.diarium.collaborate.model.request.ShareNewsfeedRequest;
import com.diarium.collaborate.model.response.NewsfeedInfoResponse;
import com.diarium.collaborate.model.response.NewsfeedResponse;
import com.diarium.collaborate.model.response.PhotoAlbumResponse;
import com.diarium.collaborate.repository.NewsfeedRepository;
import com.diarium.collaborate.service.BirthdayCardService;
import com.diarium.collaborate.service.CommentService;
import com.diarium.collaborate.service.CommunityService;
import com.diarium.collaborate.service.FileService;
import com.diarium.collaborate.service.LikeService;
import com.diarium.collaborate.service.NewsfeedService;
import com.diarium.collaborate.service.NotificationReceiverService;
import com.diarium.collaborate.service.PageService;
import com.diarium.collaborate.service.PhotoAlbumService;
import com.diarium.collaborate.service.PhotoService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;
import com.diarium.collaborate.utils.NewsfeedUtils;

@Service
class NewsfeedServiceImpl implements NewsfeedService {

	@Autowired
	private CommentService commentService;
	@Autowired
	private LikeService likeService;
	@Autowired
	private UserService userService;
	@Autowired
	private PageService pageService;
	@Autowired
	private CommunityService communityService;
	@Autowired
	private NotificationReceiverService notificationReceiverService;
	@Autowired
	private PhotoService photoService;
	@Autowired
	private PhotoAlbumService photoAlbumService;
	@Autowired
	private BirthdayCardService birthdayCardService;
	@Autowired
	private FileService fileService;

	@Autowired
	private NewsfeedRepository repository;
	
	@Override
	public List<Newsfeed> getForDashboard(Integer userId, Integer limit, Integer offset) {
		return repository.findForDashboard(userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<Newsfeed> getForMyTimeline(Integer userId, Integer limit, Integer offset) {
		return repository.findForMyTimeline(userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<Newsfeed> getForFriendsTimeline(Integer ownerId, Integer userId, Integer limit, Integer offset) {
		return repository.findForFriendsTimeline(ownerId, userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<Newsfeed> getForFollowingsTimeline(Integer ownerId, Integer userId, Integer limit, Integer offset) {
		return repository.findForFollowingsTimeline(ownerId, userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<Newsfeed> getForOthersTimeline(Integer ownerId, Integer userId, Integer limit, Integer offset) {
		return repository.findForOthersTimeline(ownerId, userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<Newsfeed> getForPage(Integer pageId, Integer userId, Integer limit, Integer offset) {
		return repository.findByPageId(pageId, userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public List<Newsfeed> getForCommunity(Integer communityId, Integer userId, Integer limit, Integer offset) {
		return repository.findByCommunityId(communityId, userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public Newsfeed getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public NewsfeedResponse create(CreateNewsfeedRequest request) throws WebCrawlingFailedException {
		Newsfeed newsfeed = new Newsfeed();
		newsfeed.setUserId(request.getUserId());
		newsfeed.setToUserId(request.getToUserId());
		newsfeed.setText(request.getText());
		newsfeed.setContentType(request.getContentType());
		newsfeed.setTypeContent(NewsfeedUtils.encodeTypeContents(request.getTypeContent()));
		newsfeed.setType(request.getType());
		newsfeed.setTypeId(request.getTypeId());
		newsfeed.setCommunityId(request.getCommunityId());
		newsfeed.setPageId(request.getPageId());
		newsfeed.setLink(NewsfeedUtils.encodeLink(request.getLink()));
		newsfeed.setTags(NewsfeedUtils.encodeTags(request.getTags()));
		newsfeed.setPrivacy(request.getPrivacy());
		newsfeed.setCreatedAt(new Date());
		newsfeed.setUpdatedAt(new Date());
		newsfeed.setEdited(0);
		newsfeed.setVideoPath(request.getVideoPath());
		
		if (request.getPageId() != 0) {
			newsfeed.setAutoLikeId("page-" + request.getPageId());
		} else {
			newsfeed.setAutoLikeId("");
		}
		
		newsfeed.setFilePath(request.getFilePath());
		newsfeed.setFilePathName(request.getFilePathName());
		newsfeed.setPinned(0);
		newsfeed = repository.save(newsfeed);
		
		if (request.getContentType().equals("image")) {
			for (String imgPath : request.getTypeContent()) {
				PhotoAlbumResponse album = photoAlbumService.getPhotoAlbumResponse(photoAlbumService.getByUserAndTitle(newsfeed.getUserId(), "posts"));
				
				if (album == null) {
					CreatePhotoAlbumRequest albumRequest = new CreatePhotoAlbumRequest();
					albumRequest.setUserId(request.getUserId());
					albumRequest.setTitle("posts");
					albumRequest.setSlug(request.getUserId() + "-posts");
					albumRequest.setDefaultPhoto(request.getTypeContent()[0]);
					album = photoAlbumService.create(albumRequest);
				}
				
				CreatePhotoRequest photo = new CreatePhotoRequest();
				photo.setUserId(newsfeed.getUserId());
				photo.setSlug("album-" + album.getId());
				photo.setPath(imgPath);
				photo.setPrivacy(newsfeed.getPrivacy());
				photo.setPageId(request.getPageId());
				photo.setPostId(newsfeed.getId());
				photoService.create(photo);
			}
		}
		
		notificationReceiverService.create(request.getUserId(), "post", newsfeed.getId());
		return getNewsfeedResponse(newsfeed, request.getUserId());
	}
	
	@Override
	public NewsfeedResponse edit(Newsfeed originalNewsfeed, EditNewsfeedRequest request) throws WebCrawlingFailedException {
		originalNewsfeed.setText(request.getText());
		originalNewsfeed.setContentType(request.getContentType());
		originalNewsfeed.setTypeContent(NewsfeedUtils.encodeTypeContents(request.getTypeContent()));
		originalNewsfeed.setType(request.getType());
		originalNewsfeed.setTypeId(request.getTypeId());
		originalNewsfeed.setLink(NewsfeedUtils.encodeLink(request.getLink()));
		originalNewsfeed.setTags(NewsfeedUtils.encodeTags(request.getTags()));
		originalNewsfeed.setPrivacy(request.getPrivacy());
		originalNewsfeed.setUpdatedAt(new Date());
		originalNewsfeed.setEdited(1);
		originalNewsfeed.setVideoPath(request.getVideoPath());
		originalNewsfeed.setFilePath(request.getFilePath());
		originalNewsfeed.setFilePathName(request.getFilePathName());
		originalNewsfeed.setPinned(0);
		originalNewsfeed = repository.save(originalNewsfeed);
		return getNewsfeedResponse(originalNewsfeed, originalNewsfeed.getUserId());
	}
	
	@Override
	public void delete(Newsfeed newsfeed) throws FileDeletionFailedException {
		deleteEmbeddedData(newsfeed);
		repository.delete(newsfeed.getId());
	}
	
	@Override
	public NewsfeedResponse share(Newsfeed originalNewsfeed, ShareNewsfeedRequest request) {
		Newsfeed newsfeed = new Newsfeed();
		newsfeed.setUserId(request.getUserId());
		newsfeed.setToUserId(request.getToUserId());
		newsfeed.setText(request.getText());
		newsfeed.setContentType(originalNewsfeed.getContentType());
		newsfeed.setTypeContent(originalNewsfeed.getTypeContent());
		newsfeed.setType(request.getType());
		newsfeed.setTypeId(request.getTypeId());
		newsfeed.setCommunityId(request.getCommunityId());
		newsfeed.setPageId(request.getPageId());
		newsfeed.setLink(originalNewsfeed.getLink());
		newsfeed.setTags(NewsfeedUtils.encodeTags(request.getTags()));
		newsfeed.setPrivacy(request.getPrivacy());
		newsfeed.setShared(1);
		newsfeed.setSharedId(originalNewsfeed.getId());
		
		if (originalNewsfeed.getSharedFrom() != null) {
			newsfeed.setSharedFrom(originalNewsfeed.getSharedFrom());
		} else {
			newsfeed.setSharedFrom(originalNewsfeed.getUserId());
		}
		
		newsfeed.setCreatedAt(new Date());
		newsfeed.setUpdatedAt(new Date());
		newsfeed.setEdited(0);
		newsfeed.setVideoPath(originalNewsfeed.getVideoPath());
		
		if (request.getPageId() != 0) {
			newsfeed.setAutoLikeId("page-" + request.getPageId());
		} else {
			newsfeed.setAutoLikeId("");
		}
		
		newsfeed.setFilePath(originalNewsfeed.getFilePath());
		newsfeed.setFilePathName(originalNewsfeed.getFilePathName());
		newsfeed.setPinned(0);
		newsfeed = repository.save(newsfeed);
		
		if (originalNewsfeed.getSharedCount() == null) {
			originalNewsfeed.setSharedCount(1);
		} else {
			originalNewsfeed.setSharedCount(originalNewsfeed.getSharedCount() + 1);
		}
		
		repository.save(originalNewsfeed);
		return getNewsfeedResponse(newsfeed, request.getUserId());
	}
	
	@Override
	public NewsfeedInfoResponse getNewsfeedInfoResponse(Newsfeed newsfeed) {
		if (newsfeed != null) {
			NewsfeedInfoResponse response = new NewsfeedInfoResponse();
			response.setId(newsfeed.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(newsfeed.getUserId())));
			response.setReceiver(userService.getUserInfoResponse(userService.getById(newsfeed.getToUserId())));
			response.setText(newsfeed.getText());
			response.setContentType(newsfeed.getContentType());
			response.setTypeContent(NewsfeedUtils.decodeTypeContents(newsfeed.getTypeContent(), newsfeed.getContentType(), CommonUtils.getCollaborateBaseUrl()));
			response.setCommunity(communityService.getCommunityInfoResponse(communityService.getById(newsfeed.getCommunityId())));
			response.setPage(pageService.getPageInfoResponse(pageService.getById(newsfeed.getPageId())));
			response.setLink(NewsfeedUtils.decodeLink(newsfeed.getLink()));
			response.setTags(NewsfeedUtils.decodeTags(newsfeed.getTags()));
			response.setPrivacy(newsfeed.getPrivacy());
			response.setShared(newsfeed.getShared());
			
			if (newsfeed.getSharedFrom() != null) {
				response.setSharedFrom(userService.getUserInfoResponse(userService.getById(newsfeed.getSharedFrom())));
			}
			
			response.setSharedCount(newsfeed.getSharedCount());
			response.setCreatedAt(CommonUtils.getDuration(newsfeed.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(newsfeed.getUpdatedAt().getTime()));
			response.setEdited(newsfeed.getEdited());
			
			if (newsfeed.getVideoPath() != null) {
				if (!newsfeed.getVideoPath().isEmpty()) {
					response.setVideoPath(CommonUtils.getCollaborateBaseUrl() + newsfeed.getVideoPath());
				}
			}
			
			if (newsfeed.getFilePath() != null) {
				if (!newsfeed.getFilePath().isEmpty()) {
					response.setFilePath(CommonUtils.getCollaborateBaseUrl() + newsfeed.getFilePath());
				}
			}
			
			response.setFilePathName(newsfeed.getFilePathName());
			
			response.setCommentsCount(commentService.countByType("post", newsfeed.getId()));
			response.setLikesCount(likeService.countByType("post", newsfeed.getId()));
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public NewsfeedResponse getNewsfeedResponse(Newsfeed newsfeed, Integer userId) {
		if (newsfeed != null) {
			NewsfeedResponse response = new NewsfeedResponse();
			response.setId(newsfeed.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(newsfeed.getUserId())));
			response.setReceiver(userService.getUserInfoResponse(userService.getById(newsfeed.getToUserId())));
			response.setText(newsfeed.getText());
			response.setContentType(newsfeed.getContentType());
			response.setTypeContent(NewsfeedUtils.decodeTypeContents(newsfeed.getTypeContent(), newsfeed.getContentType(), CommonUtils.getCollaborateBaseUrl()));
			response.setCommunity(communityService.getCommunityInfoResponse(communityService.getById(newsfeed.getCommunityId())));
			response.setPage(pageService.getPageInfoResponse(pageService.getById(newsfeed.getPageId())));
			response.setLink(NewsfeedUtils.decodeLink(newsfeed.getLink()));
			response.setTags(NewsfeedUtils.decodeTags(newsfeed.getTags()));
			response.setPrivacy(newsfeed.getPrivacy());
			response.setShared(newsfeed.getShared());
			
			if (newsfeed.getSharedId() != null && newsfeed.getSharedFrom() != null) {
				response.setSharedSource(getNewsfeedInfoResponse(repository.findOne(newsfeed.getSharedId())));
				response.setSharedFrom(userService.getUserInfoResponse(userService.getById(newsfeed.getSharedFrom())));
			}
			
			response.setSharedCount(newsfeed.getSharedCount());
			response.setCreatedAt(CommonUtils.getDuration(newsfeed.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(newsfeed.getUpdatedAt().getTime()));
			response.setEdited(newsfeed.getEdited());
			
			if (newsfeed.getVideoPath() != null) {
				if (!newsfeed.getVideoPath().isEmpty()) {
					response.setVideoPath(CommonUtils.getCollaborateBaseUrl() + newsfeed.getVideoPath());
				}
			}
			
			if (newsfeed.getFilePath() != null) {
				if (!newsfeed.getFilePath().isEmpty()) {
					response.setFilePath(CommonUtils.getCollaborateBaseUrl() + newsfeed.getFilePath());
				}
			}
			
			response.setFilePathName(newsfeed.getFilePathName());
			
			response.setCommentsCount(commentService.countByType("post", newsfeed.getId()));
			List<Comment> comments = commentService.getByType("post", newsfeed.getId(), 10, 0);
			
			for (Comment comment : comments) {
				response.getComments().add(commentService.getCommentResponse(comment, userId));
			}
			
			response.setExpressionsCount(likeService.countByType("post", newsfeed.getId()));
			List<Like> expressions = likeService.getByType("post", newsfeed.getId(), 10, 0);
			
			for (Like expression : expressions) {
				response.getExpressions().add(likeService.getLikeResponse(expression));
			}

			response.setLikesCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 1));
			response.setLoveCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 2));
			response.setHahaCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 3));
			response.setWowCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 4));
			response.setSadCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 5));
			response.setAngryCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 6));
			response.setApplauseCount(likeService.countByTypeAndEmoji("post", newsfeed.getId(), 7));
			
			Like like = likeService.getByUserAndType(userId, "post", newsfeed.getId());
			
			if (like == null) {
				response.setAlreadyLike(false);
			} else {
				response.setGivenExpression(like.getEmoji());
				response.setAlreadyLike(true);
			}
			
			response.setReceiveNotif(notificationReceiverService.getByUserAndType(userId, "post", newsfeed.getId()) != null);
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public void deleteEmbeddedData(Newsfeed newsfeed) throws FileDeletionFailedException {
		List<Comment> comments = commentService.getByType("post", newsfeed.getId());
		List<Like> likes = likeService.getByType("post", newsfeed.getId());
		List<Photo> photos = photoService.getByPost(newsfeed.getId());
		List<NotificationReceiver> nrs = notificationReceiverService.getByType("post", newsfeed.getId());
		List<Newsfeed> shareds = repository.findBySharedId(newsfeed.getId());
		BirthdayCard birthdayCard = birthdayCardService.getByNewsfeed(newsfeed.getId());
		
		for (Comment comment : comments) {
			commentService.deleteEmbeddedData(comment);
		}
		
		for (Like like : likes) {
			likeService.delete(like);
		}

		fileService.deleteFile(newsfeed.getFilePath(), false);
		fileService.deleteFile(newsfeed.getVideoPath(), false);
		
		for (Photo photo : photos) {
			photoService.delete(photo);
		}
		
		for (NotificationReceiver nr : nrs) {
			notificationReceiverService.delete(nr);
		}
		
		for (Newsfeed shared : shareds) {
			deleteEmbeddedData(shared);
			repository.delete(shared.getId());
		}
		
		if (birthdayCard != null) {
			birthdayCardService.delete(birthdayCard.getId());
		}
	}
}
