package com.diarium.collaborate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.CommunityMember;
import com.diarium.collaborate.model.response.CommunityMemberResponse;
import com.diarium.collaborate.repository.CommunityMemberRepository;
import com.diarium.collaborate.service.CommunityMemberService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class CommunityMemberServiceImpl implements CommunityMemberService {

	@Autowired
	private UserService userService;

	@Autowired
	private CommunityMemberRepository repository;
	
	@Override
	public List<CommunityMember> getByCommunity(Integer communityId) {
		return repository.findByCommunityId(communityId);
	}
	
	@Override
	public CommunityMemberResponse getCommunityMemberResponse(CommunityMember communityMember) {
		if (communityMember != null) {
			CommunityMemberResponse response = new CommunityMemberResponse();
			response.setId(communityMember.getId());
			response.setUser(userService.getUserInfoResponse(userService.getById(communityMember.getUserId())));
			response.setCreatedAt(CommonUtils.getDuration(communityMember.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(communityMember.getUpdatedAt().getTime()));
			return response;
		} else {
			return null;
		}
	}
}
