package com.diarium.collaborate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Notification;
import com.diarium.collaborate.model.response.NotificationResponse;
import com.diarium.collaborate.repository.NotificationRepository;
import com.diarium.collaborate.service.NotificationService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class NotificationServiceImpl implements NotificationService {

	@Autowired
	private UserService userService;
	
	@Autowired
	public NotificationRepository repository;
	
	@Override
	public List<Notification> getByToUser(Integer toUserId, Integer limit, Integer offset) {
		return repository.findByToUserId(toUserId, new Integer(offset*limit), limit);
	}
	
	@Override
	public Notification getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public Integer countByUser(Integer toUserId) {
		return repository.countByToUserId(toUserId);
	}
	
	@Override
	public void delete(Notification notification) {
		repository.delete(notification.getId());
	}
	
	@Override
	public NotificationResponse getNotificationResponse(Notification notification) {
		if (notification != null) {
			NotificationResponse response = new NotificationResponse();
			response.setId(notification.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(notification.getUserId())));
			response.setToUserId(userService.getUserInfoResponse(userService.getById(notification.getToUserId())));
			response.setTitle(notification.getTitle());
			response.setContent(notification.getContent());
			response.setData(notification.getData());
			response.setSeen(notification.getSeen());
			response.setCreatedAt(CommonUtils.getDuration(notification.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(notification.getUpdatedAt().getTime()));
			return response;
		} else {
			return null;
		}
	}
}
