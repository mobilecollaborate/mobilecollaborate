package com.diarium.collaborate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Page;
import com.diarium.collaborate.model.response.PageInfoResponse;
import com.diarium.collaborate.model.response.PageResponse;
import com.diarium.collaborate.repository.PageRepository;
import com.diarium.collaborate.service.LikeService;
import com.diarium.collaborate.service.PageService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class PageServiceImpl implements PageService {

	@Autowired
	private LikeService likeService;
	@Autowired
	private UserService userService;
	
	@Autowired
	private PageRepository repository;
	
	@Override
	public List<Page> getByUser(Integer userId, Integer limit, Integer offset) {
		return repository.findLikedPage(userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public Page getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public PageInfoResponse getPageInfoResponse(Page page) {
		if (page != null) {
			PageInfoResponse response = new PageInfoResponse();
			response.setId(page.getId());
			response.setTitle(page.getTitle());
			response.setSlug(page.getSlug());
			
			if (page.getLogo() != null) {
				if (!page.getLogo().isEmpty()) {
					response.setLogo(CommonUtils.getCollaborateBaseUrl() + page.getLogo());
				}
			}
			
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public PageResponse getPageResponse(Page page) {
		if (page != null) {
			PageResponse response = new PageResponse();
			response.setId(page.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(page.getUserId())));
			response.setCategoryId(page.getCategoryId());
			response.setTitle(page.getTitle());
			response.setSlug(page.getSlug());
			response.setDescription(page.getDescription());
			response.setInfo(page.getInfo());
			response.setWebsite(page.getWebsite());
			
			if (page.getLogo() != null) {
				if (!page.getLogo().isEmpty()) {
					response.setLogo(CommonUtils.getCollaborateBaseUrl() + page.getLogo());
				}
			}
			
			if (page.getCover() != null) {
				if (!page.getCover().isEmpty()) {
					response.setCover(CommonUtils.getCollaborateBaseUrl() + page.getCover());
				}
			}
			
			response.setVerified(page.getVerified());
			response.setCreatedAt(CommonUtils.getDuration(page.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(page.getUpdatedAt().getTime()));
			
			if (page.getOriginalCovar() != null) {
				if (!page.getOriginalCovar().isEmpty()) {
					response.setOriginalCovar(CommonUtils.getCollaborateBaseUrl() + page.getOriginalCovar());
				}
			}
			
			response.setLikeCount(likeService.countByType("page", page.getId()));
			return response;
		} else {
			return null;
		}
	}
}
