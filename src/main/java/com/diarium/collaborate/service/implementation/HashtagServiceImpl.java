package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Hashtag;
import com.diarium.collaborate.repository.HashtagRepository;
import com.diarium.collaborate.service.HashtagService;

@Service
class HashtagServiceImpl implements HashtagService {
	
	@Autowired
	private HashtagRepository repository;

	@Override
	public List<Hashtag> getTopTen() {
		return repository.findTop10ByOrderByCountDesc();
	};
	
	@Override
	public Hashtag add(String hash) {
		Hashtag hashtag = repository.findOneByHash(hash);
		
		if (hashtag == null) {
			hashtag = new Hashtag();
			hashtag.setHash(hashtag.getHash());
			hashtag.setCount(1);
			hashtag.setCreatedAt(new Date());
			hashtag.setUpdatedAt(new Date());
		} else {
			hashtag.setCount(hashtag.getCount() + 1);
		}
		
		return repository.save(hashtag);
	}
}
