package com.diarium.collaborate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Community;
import com.diarium.collaborate.model.response.CommunityInfoResponse;
import com.diarium.collaborate.model.response.CommunityResponse;
import com.diarium.collaborate.repository.CommunityRepository;
import com.diarium.collaborate.service.CommunityService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class CommunityServiceImpl implements CommunityService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private CommunityRepository repository;
	
	@Override
	public List<Community> getByUser(Integer userId, Integer limit, Integer offset) {
		return repository.findMyCommunity(userId, new Integer(offset*limit), limit);
	}
	
	@Override
	public Community getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public CommunityInfoResponse getCommunityInfoResponse(Community community) {
		if (community != null) {
			CommunityInfoResponse response = new CommunityInfoResponse();
			response.setId(community.getId());
			response.setTitle(community.getTitle());
			response.setSlug(community.getSlug());
			
			if (community.getLogo() != null) {
				if (!community.getLogo().isEmpty()) {
					response.setLogo(CommonUtils.getCollaborateBaseUrl() + community.getLogo());
				}
			} else {
				response.setLogo(CommonUtils.getCollaborateBaseUrl() + "themes/frontend/default/assets/images/community/cover.jpg");
			}
			
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public CommunityResponse getCommunityResponse(Community community) {
		if (community != null) {
			CommunityResponse response = new CommunityResponse();
			response.setId(community.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(community.getUserId())));
			response.setTitle(community.getTitle());
			response.setSlug(community.getSlug());
			response.setDescription(community.getDescription());
			response.setInfo(community.getInfo());
			response.setModerators(community.getModerators());
			response.setPrivacy(community.getPrivacy());
			response.setCanJoin(community.getCanJoin());
			response.setCanPost(community.getCanPost());
			response.setCanInvite(community.getCanInvite());
			response.setSearchable(community.getSearchable());
			
			if (community.getLogo() != null) {
				if (!community.getLogo().isEmpty()) {
					response.setLogo(CommonUtils.getCollaborateBaseUrl() + community.getLogo());
				}
			} else {
				response.setLogo(CommonUtils.getCollaborateBaseUrl() + "themes/frontend/default/assets/images/community/cover.jpg");
			}
			
			response.setCreatedAt(CommonUtils.getDuration(community.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(community.getUpdatedAt().getTime()));
			return response;
		} else {
			return null;
		}
	}
}
