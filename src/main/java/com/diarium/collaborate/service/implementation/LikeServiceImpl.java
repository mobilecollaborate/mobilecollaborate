package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Like;
import com.diarium.collaborate.model.request.EditExpressionRequest;
import com.diarium.collaborate.model.request.GiveExpressionRequest;
import com.diarium.collaborate.model.response.LikeResponse;
import com.diarium.collaborate.repository.LikeRepository;
import com.diarium.collaborate.service.LikeService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class LikeServiceImpl implements LikeService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private LikeRepository repository;
	
	@Override
	public List<Like> getByType(String type, Integer typeId) {
		return repository.findByTypeAndTypeId(type, typeId);
	}
	
	@Override
	public List<Like> getByType(String type, Integer typeId, Integer limit, Integer offset) {
		return repository.findByTypeAndTypeIdOrderByCreatedAtDesc(type, typeId, new PageRequest(offset, limit, Sort.Direction.DESC, "createdAt"));
	}
	
	@Override
	public Like getByUserAndType(Integer userId, String type, Integer typeId) {
		return repository.findByUserIdAndTypeAndTypeId(userId, type, typeId);
	}
	
	@Override
	public Like getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public Integer countByType(String type, Integer typeId) {
		return repository.countByTypeAndTypeId(type, typeId);
	}
	
	@Override
	public Integer countByTypeAndEmoji(String type, Integer typeId, Integer emoji) {
		return repository.countByTypeAndTypeIdAndEmoji(type, typeId, emoji);
	}
	
	@Override
	public LikeResponse create(GiveExpressionRequest request) {
		Like like = new Like();
		like.setUserId(request.getUserId());
		like.setType(request.getType());
		like.setTypeId(request.getTypeId());
		like.setEmoji(request.getEmoji());
		like.setCreatedAt(new Date());
		like.setUpdatedAt(new Date());
		like = repository.save(like);
		return getLikeResponse(like);
	}
	
	@Override
	public LikeResponse edit(Like originalLike, EditExpressionRequest request) {
		originalLike.setEmoji(request.getEmoji());
		originalLike.setUpdatedAt(new Date());
		originalLike = repository.save(originalLike);
		return getLikeResponse(originalLike);
	}
	
	@Override
	public void delete(Like like) {
		repository.delete(like.getId());
	}
	
	@Override
	public LikeResponse getLikeResponse(Like like) {
		if (like != null) {
			LikeResponse response = new LikeResponse();
			response.setId(like.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(like.getUserId())));
			response.setCreatedAt(CommonUtils.getDuration(like.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(like.getUpdatedAt().getTime()));
			response.setEmoji(like.getEmoji());
			return response;
		} else {
			return null;
		}
	}
}
