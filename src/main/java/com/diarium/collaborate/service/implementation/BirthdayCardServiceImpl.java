package com.diarium.collaborate.service.implementation;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.entity.BirthdayCard;
import com.diarium.collaborate.model.request.CreateBirthdayCardRequest;
import com.diarium.collaborate.model.request.CreateNewsfeedRequest;
import com.diarium.collaborate.model.response.NewsfeedResponse;
import com.diarium.collaborate.repository.BirthdayCardRepository;
import com.diarium.collaborate.service.BirthdayCardService;
import com.diarium.collaborate.service.NewsfeedService;

@Service
class BirthdayCardServiceImpl implements BirthdayCardService {

	@Autowired
	private NewsfeedService newsfeedService;
	
	@Autowired
	private BirthdayCardRepository repository;
	
	@Override
	public BirthdayCard getOne(Integer userId, Integer toUserId) {
		return repository.findOneByUserIdAndToUserIdAndYear(userId, toUserId, LocalDate.now().getYear());
	}
	
	@Override
	public BirthdayCard getByNewsfeed(Integer newsfeedId) {
		return repository.findOneByPostId(newsfeedId);
	}
	
	@Override
	public NewsfeedResponse create(CreateBirthdayCardRequest request) throws WebCrawlingFailedException {
		CreateNewsfeedRequest newsfeed = new CreateNewsfeedRequest();
		newsfeed.setUserId(request.getUserId());
		newsfeed.setToUserId(request.getToUserId());
		newsfeed.setText(request.getText());
		newsfeed.setContentType(request.getContentType());
		newsfeed.setTypeContent(request.getTypeContent());
		newsfeed.setType("user-timeline");
		newsfeed.setTypeId(0);
		newsfeed.setCommunityId(0);
		newsfeed.setPageId(0);
		newsfeed.setLink(request.getLink());
		newsfeed.setTags(request.getTags());
		newsfeed.setPrivacy(1);
		newsfeed.setVideoPath(request.getVideoPath());
		newsfeed.setFilePath(request.getFilePath());
		newsfeed.setFilePathName(request.getFilePathName());
		NewsfeedResponse newsfeedResponse = newsfeedService.create(newsfeed);
		
		BirthdayCard birthdayCard = new BirthdayCard();
		birthdayCard.setUserId(request.getUserId());
		birthdayCard.setToUserId(request.getToUserId());
		birthdayCard.setPostId(newsfeedResponse.getId());
		birthdayCard.setYear(LocalDate.now().getYear());
		repository.save(birthdayCard);
		return newsfeedResponse;
	}
	
	@Override
	public void delete(Integer id) {
		repository.delete(id);
	}
}
