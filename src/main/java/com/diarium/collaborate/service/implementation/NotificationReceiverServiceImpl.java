package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.NotificationReceiver;
import com.diarium.collaborate.repository.NotificationReceiverRepository;
import com.diarium.collaborate.service.NotificationReceiverService;

@Service
class NotificationReceiverServiceImpl implements NotificationReceiverService {

	@Autowired
	private NotificationReceiverRepository repository;
	
	@Override
	public List<NotificationReceiver> getByType(String type, Integer typeId) {
		return repository.findByTypeAndTypeId(type, typeId);
	}
	
	@Override
	public NotificationReceiver getByUserAndType(Integer userId, String type, Integer typeId) {
		return repository.findOneByUserIdAndTypeAndTypeId(userId, type, typeId);
	}
	
	@Override
	public void create(Integer userId, String type, Integer typeId) {
		NotificationReceiver notificationReceiver = new NotificationReceiver();
		notificationReceiver.setUserId(userId);
		notificationReceiver.setType(type);
		notificationReceiver.setTypeId(typeId);
		notificationReceiver.setCreatedAt(new Date());
		notificationReceiver.setUpdatedAt(new Date());
		notificationReceiver = repository.save(notificationReceiver);
	}
	
	@Override
	public void delete(NotificationReceiver notificationReceiver) {
		repository.delete(notificationReceiver.getId());
	}
}
