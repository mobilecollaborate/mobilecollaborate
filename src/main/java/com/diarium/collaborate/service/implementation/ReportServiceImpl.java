package com.diarium.collaborate.service.implementation;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Report;
import com.diarium.collaborate.repository.ReportRepository;
import com.diarium.collaborate.service.ReportService;

@Service
class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportRepository repository;
	
	@Override
	public void create(Report report) {
		report.setCreatedAt(new Date());
		report.setUpdatedAt(new Date());
		repository.save(report);
	}
}
