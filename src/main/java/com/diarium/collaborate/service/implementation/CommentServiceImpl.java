package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.model.entity.Comment;
import com.diarium.collaborate.model.entity.Like;
import com.diarium.collaborate.model.request.AddCommentRequest;
import com.diarium.collaborate.model.request.EditCommentRequest;
import com.diarium.collaborate.model.response.CommentResponse;
import com.diarium.collaborate.repository.CommentRepository;
import com.diarium.collaborate.service.CommentService;
import com.diarium.collaborate.service.FileService;
import com.diarium.collaborate.service.LikeService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class CommentServiceImpl implements CommentService {

	@Autowired
	private LikeService likeService;
	@Autowired
	private UserService userService;
	@Autowired
	private FileService fileService;
	
	@Autowired
	private CommentRepository repository;
	
	@Override
	public List<Comment> getByType(String type, Integer typeId) {
		return repository.findByTypeAndTypeId(type, typeId);
	}
	
	@Override
	public List<Comment> getByType(String type, Integer typeId, Integer limit, Integer offset) {
		return repository.findByTypeAndTypeIdOrderByCreatedAtAsc(type, typeId, new PageRequest(offset, limit, Sort.Direction.ASC, "createdAt"));
	}
	
	@Override
	public Comment getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public Integer countByType(String type, Integer typeId) {
		return repository.countByTypeAndTypeId(type, typeId);
	}
	
	@Override
	public CommentResponse create(AddCommentRequest request) {
		Comment comment = new Comment();
		comment.setUserId(request.getUserId());
		comment.setText(request.getText());
		comment.setType(request.getType());
		comment.setTypeId(request.getTypeId());
		comment.setImgPath(request.getImgPath());
		comment.setCreatedAt(new Date());
		comment.setUpdatedAt(new Date());
		comment = repository.save(comment);
		return getCommentResponse(comment, request.getUserId());
	}
	
	@Override
	public CommentResponse edit(Comment originalComment, EditCommentRequest request) {
		originalComment.setText(request.getText());
		originalComment.setImgPath(request.getImgPath());
		originalComment.setUpdatedAt(new Date());
		originalComment = repository.save(originalComment);
		return getCommentResponse(originalComment, originalComment.getUserId());
	}
	
	@Override
	public void delete(Comment comment) throws FileDeletionFailedException {
		deleteEmbeddedData(comment);
		repository.delete(comment.getId());
	}
	
	@Override
	public CommentResponse getCommentResponse(Comment comment, Integer userId) {
		if (comment != null) {
			CommentResponse response = new CommentResponse();
			response.setId(comment.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(comment.getUserId())));
			response.setText(comment.getText());
			
			if (comment.getImgPath() != null) {
				if (!comment.getImgPath().isEmpty()) {
					response.setImgPath(CommonUtils.getCollaborateBaseUrl() + comment.getImgPath());
				}
			}
			
			response.setCreatedAt(CommonUtils.getDuration(comment.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(comment.getUpdatedAt().getTime()));
			
			response.setLikesCount(likeService.countByType("comment", comment.getId()));
			List<Like> likes = likeService.getByType("comment", comment.getId(), 10, 0);
			
			for (Like like : likes) {
				response.getLikes().add(likeService.getLikeResponse(like));
			}
			
			Like like = likeService.getByUserAndType(userId, "comment", comment.getId());
			
			if (like == null) {
				response.setAlreadyLike(false);
			} else {
				response.setGivenExpression(like.getEmoji());
				response.setAlreadyLike(true);
			}
			
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public void deleteEmbeddedData(Comment comment) throws FileDeletionFailedException {
		List<Like> likes = likeService.getByType("comment", comment.getId());
		
		for (Like like : likes) {
			likeService.delete(like);
		}
		
		fileService.deleteFile(comment.getImgPath(), true);
	}
}
