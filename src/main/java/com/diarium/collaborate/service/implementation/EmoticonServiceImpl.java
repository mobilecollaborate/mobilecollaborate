package com.diarium.collaborate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Emoticon;
import com.diarium.collaborate.repository.EmoticonRepository;
import com.diarium.collaborate.service.EmoticonService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class EmoticonServiceImpl implements EmoticonService {
	
	@Autowired
	private EmoticonRepository repository;
	
	@Override
	public List<Emoticon> getAllEmoticons() {
		List<Emoticon> emoticons = repository.findAll();
		
		for (Emoticon emoticon : emoticons) {
			emoticon.setPath(CommonUtils.getCollaborateBaseUrl() + emoticon.getPath());
		}
		
		return emoticons;
	}
}
