package com.diarium.collaborate.service.implementation;

import org.springframework.stereotype.Service;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.service.FileService;
import com.diarium.collaborate.utils.FileUtils;

@Service
class FileServiceImpl implements FileService {
	
	@Override
	public void deleteFile(String path, boolean isImage) throws FileDeletionFailedException {
		try {
			if (path != null) {
				if (!path.isEmpty()) {
					if (isImage) {
						FileUtils.getMinioClient().removeObject(FileUtils.getMinioBucketName(), String.format(path, 50));
						FileUtils.getMinioClient().removeObject(FileUtils.getMinioBucketName(), String.format(path, 200));
						FileUtils.getMinioClient().removeObject(FileUtils.getMinioBucketName(), String.format(path, 600));
						FileUtils.getMinioClient().removeObject(FileUtils.getMinioBucketName(), String.format(path, 960));
					} else {
						FileUtils.getMinioClient().removeObject(FileUtils.getMinioBucketName(), path);
					}
				}
			}
		} catch (Exception e) {
			throw new FileDeletionFailedException();
		}
	}
}
