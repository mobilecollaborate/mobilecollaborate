package com.diarium.collaborate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.Expression;
import com.diarium.collaborate.repository.ExpressionRepository;
import com.diarium.collaborate.service.ExpressionService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class ExpressionServiceImpl implements ExpressionService {

	@Autowired
	private ExpressionRepository repository;
	
	@Override
	public List<Expression> getAllExpressions() {
		List<Expression> expressions = repository.findAll();
		
		for (Expression expression : expressions) {
			expression.setPath(CommonUtils.getCollaborateBaseUrl() + expression.getPath());
		}
		
		return expressions;
	}
}
