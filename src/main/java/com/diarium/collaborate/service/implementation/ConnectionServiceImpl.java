package com.diarium.collaborate.service.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.enumtype.ConfirmationStatus;
import com.diarium.collaborate.enumtype.ConnectionWay;
import com.diarium.collaborate.model.entity.Connection;
import com.diarium.collaborate.model.request.CreateConnectionRequest;
import com.diarium.collaborate.model.response.FriendRequestResponse;
import com.diarium.collaborate.model.response.ConnectionResponse;
import com.diarium.collaborate.repository.ConnectionRepository;
import com.diarium.collaborate.service.ConnectionService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class ConnectionServiceImpl implements ConnectionService {

	@Autowired
	private UserService userService;

	@Autowired
	private ConnectionRepository repository;
	
	@Override
	public List<Connection> getFriends(Integer userId, Integer limit, Integer offset) {
		return repository.findFriends(userId, limit, new Integer(offset*limit));
	}
	
	@Override
	public List<Connection> getFriendRequests(Integer userId, Integer limit, Integer offset) {
		return repository.findFriendRequests(userId, limit, new Integer(offset*limit));
	}
	
	@Override
	public List<Connection> getFollowings(Integer userId, Integer limit, Integer offset) {
		return repository.findFollowings(userId, limit, new Integer(offset*limit));
	}
	
	@Override
	public List<Connection> getFollowers(Integer userId, Integer limit, Integer offset) {
		return repository.findFollowers(userId, limit, new Integer(offset*limit));
	}
	
	@Override
	public List<Connection> getFriendConnections(Integer userId, Integer toUserId) {
		List<Connection> connections = new ArrayList<Connection>();
		connections.add(repository.findOneByUserIdAndToUserIdAndWayAndConfirmed(userId, toUserId, ConnectionWay.FRIEND.getValue(), ConfirmationStatus.CONFIRMED.getValue()));
		connections.add(repository.findOneByUserIdAndToUserIdAndWayAndConfirmed(toUserId, userId, ConnectionWay.FRIEND.getValue(), ConfirmationStatus.CONFIRMED.getValue()));
		connections.add(repository.findOneByUserIdAndToUserIdAndWay(userId, toUserId, ConnectionWay.FOLLOW.getValue()));
		connections.add(repository.findOneByUserIdAndToUserIdAndWay(toUserId, userId, ConnectionWay.FOLLOW.getValue()));
		return connections;
	}
	
	@Override
	public Connection getFriendRequestConnection(Integer userId, Integer toUserId) {
		return repository.findOneByUserIdAndToUserIdAndWayAndConfirmed(userId, toUserId, ConnectionWay.FRIEND.getValue(), ConfirmationStatus.NOT_YET_CONFIRMED.getValue());
	}
	
	@Override
	public Connection getFollowingConnection(Integer userId, Integer toUserId) {
		return repository.findOneByUserIdAndToUserIdAndWay(userId, toUserId, ConnectionWay.FOLLOW.getValue());
	}
	
	@Override
	public Integer countFriends(Integer userId) {
		return repository.countFriends(userId);
	}
	
	@Override
	public Integer countFriendRequests(Integer userId) {
		return repository.countFriendRequests(userId);
	}
	
	@Override
	public Integer countFollowings(Integer userId) {
		return repository.countFollowings(userId);
	}
	
	@Override
	public Integer countFollowers(Integer userId) {
		return repository.countFollowers(userId);
	}
	
	@Override
	public void create(CreateConnectionRequest request, Integer way, Integer confirmed) {
		Connection connection = new Connection();
		connection.setUserId(request.getUserId());
		connection.setToUserId(request.getToUserId());
		connection.setWay(way);
		connection.setConfirmed(confirmed);
		connection.setCreatedAt(new Date());
		connection.setUpdatedAt(new Date());
		repository.save(connection);
	}
	
	@Override
	public void edit(Connection connection, Integer way, Integer confirmed) {
		connection.setWay(way);
		connection.setConfirmed(confirmed);
		connection.setUpdatedAt(new Date());
		repository.save(connection);
	}
	
	@Override
	public void delete(Connection connection) {
		repository.delete(connection.getId());
	}
	
	@Override
	public ConnectionResponse getConnectionResponse(Connection connection, Integer userId) {
		if (connection != null) {
			ConnectionResponse response = new ConnectionResponse();
			response.setId(connection.getId());
			
			if (userId.equals(connection.getUserId())) {
				response.setConnection(userService.getUserInfoResponse(userService.getById(connection.getToUserId())));
			} else if (userId.equals(connection.getToUserId())) {
				response.setConnection(userService.getUserInfoResponse(userService.getById(connection.getUserId())));
			}
			
			response.setCreatedAt(CommonUtils.getDuration(connection.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(connection.getUpdatedAt().getTime()));
			return response;
		} else {
			return null;
		}
	}

	@Override
	public FriendRequestResponse getFriendRequestResponse(Connection connection) {
		if (connection != null) {
			FriendRequestResponse response = new FriendRequestResponse();
			response.setId(connection.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(connection.getUserId())));
			response.setCreatedAt(CommonUtils.getDuration(connection.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(connection.getUpdatedAt().getTime()));
			return response;
		} else {
			return null;
		}
	}
	
	@Override
	public boolean isAlreadyFollower(Integer userId, Integer toUserId) {
		Connection connections = repository.findOneByUserIdAndToUserIdAndWay(userId, toUserId, ConnectionWay.FOLLOW.getValue());
		return (connections != null);
	}
	
	@Override
	public boolean isAlreadyFriends(Integer userId, Integer toUserId) {
		List<Connection> connections = new ArrayList<Connection>();
		connections.add(repository.findOneByUserIdAndToUserIdAndWayAndConfirmed(userId, toUserId, ConnectionWay.FRIEND.getValue(), ConfirmationStatus.CONFIRMED.getValue()));
		connections.add(repository.findOneByUserIdAndToUserIdAndWayAndConfirmed(toUserId, userId, ConnectionWay.FRIEND.getValue(), ConfirmationStatus.CONFIRMED.getValue()));
		return !connections.isEmpty();
	}
}
