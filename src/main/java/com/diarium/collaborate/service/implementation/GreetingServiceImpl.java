package com.diarium.collaborate.service.implementation;

import java.time.LocalTime;
import java.time.ZoneId;

import org.springframework.stereotype.Service;

import com.diarium.collaborate.enumtype.DayTime;
import com.diarium.collaborate.model.response.GreetingResponse;
import com.diarium.collaborate.service.GreetingService;

@Service
class GreetingServiceImpl implements GreetingService {

	@Override
	public GreetingResponse getGreetingResponse(String name, String timeZone) {
		if (name != null || timeZone != null) {
			GreetingResponse response = new GreetingResponse();
			LocalTime now = LocalTime.now(ZoneId.of(timeZone));
			DayTime dayTime = null;
			
			if (now.getHour() >= DayTime.MORNING.getStartHour() && now.getHour() <= DayTime.MORNING.getEndHour()) {
				dayTime = DayTime.MORNING;
			} else if (now.getHour() >= DayTime.AFTERNOON.getStartHour() && now.getHour() <= DayTime.AFTERNOON.getEndHour()) {
				dayTime = DayTime.AFTERNOON;
			} else if (now.getHour() >= DayTime.EVENING.getStartHour() || now.getHour() <= DayTime.EVENING.getEndHour()) {
				dayTime = DayTime.EVENING;
			}
			
			response.setGreeting(dayTime.getGreeting() + ", " + name + ".");
			response.setTimeZone(timeZone);
			response.setLocalTime(now.getHour() + ":" + now.getMinute());
			return response;
		} else {
			return null;
		}
	}
}
