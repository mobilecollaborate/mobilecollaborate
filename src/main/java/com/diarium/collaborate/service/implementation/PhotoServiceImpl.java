package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.model.entity.Photo;
import com.diarium.collaborate.model.request.CreatePhotoRequest;
import com.diarium.collaborate.model.response.PhotoResponse;
import com.diarium.collaborate.repository.PhotoRepository;
import com.diarium.collaborate.service.FileService;
import com.diarium.collaborate.service.NewsfeedService;
import com.diarium.collaborate.service.PageService;
import com.diarium.collaborate.service.PhotoService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class PhotoServiceImpl implements PhotoService {

	@Autowired
	private UserService userService;
	@Autowired
	private PageService pageService;
	@Autowired
	private NewsfeedService newsfeedService;
	@Autowired
	private FileService fileService;
	
	@Autowired
	private PhotoRepository repository;
	
	@Override
	public List<Photo> getByUser(Integer userId, Integer limit, Integer offset) {
		return repository.findByUserId(userId, new PageRequest(offset, limit, Sort.Direction.DESC, "createdAt"));
	}
	
	@Override
	public List<Photo> getByPost(Integer postId) {
		return repository.findByPostId(postId);
	}
	
	@Override
	public Photo getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public Integer countByUser(Integer userId) {
		return repository.countByUserId(userId);
	}
	
	@Override
	public void create(CreatePhotoRequest request) {
		Photo photo = new Photo();
		photo.setUserId(request.getUserId());
		photo.setSlug(request.getSlug());
		photo.setPath(request.getPath());
		photo.setCreatedAt(new Date());
		photo.setUpdatedAt(new Date());
		photo.setPrivacy(request.getPrivacy());
		photo.setPageId(request.getPageId());
		photo.setPostId(request.getPostId());
		repository.save(photo);
	}
	
	@Override
	public void delete(Photo photo) throws FileDeletionFailedException {
		fileService.deleteFile(photo.getPath(), true);
		repository.delete(photo.getId());
	}
	
	@Override
	public PhotoResponse getPhotoResponse(Photo photo) {
		if (photo != null) {
			PhotoResponse response = new PhotoResponse();
			response.setId(photo.getId());
			response.setOwner(userService.getUserInfoResponse(userService.getById(photo.getUserId())));
			response.setSlug(photo.getSlug());
			response.setPath(CommonUtils.getCollaborateBaseUrl() + photo.getPath());
			response.setCreatedAt(CommonUtils.getDuration(photo.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(photo.getUpdatedAt().getTime()));
			response.setPage(pageService.getPageInfoResponse(pageService.getById(photo.getId())));
			response.setPrivacy(photo.getPrivacy());
			response.setNewsfeed(newsfeedService.getNewsfeedInfoResponse(newsfeedService.getById(photo.getPostId())));
			return response;
		} else {
			return null;
		}
	}
}
