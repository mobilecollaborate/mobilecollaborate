package com.diarium.collaborate.service.implementation;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.diarium.collaborate.model.entity.PhotoAlbum;
import com.diarium.collaborate.model.request.CreatePhotoAlbumRequest;
import com.diarium.collaborate.model.response.PhotoAlbumResponse;
import com.diarium.collaborate.repository.PhotoAlbumRepository;
import com.diarium.collaborate.service.PhotoAlbumService;
import com.diarium.collaborate.service.UserService;
import com.diarium.collaborate.utils.CommonUtils;

@Service
class PhotoAlbumServiceImpl implements PhotoAlbumService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private PhotoAlbumRepository repository;
	
	@Override
	public List<PhotoAlbum> getByUser(Integer userId, Integer limit, Integer offset) {
		return repository.findByUserId(userId, new PageRequest(offset, limit, Sort.Direction.DESC, "createdAt"));
	}
	
	@Override
	public PhotoAlbum getByUserAndTitle(Integer userId, String title) {
		return repository.findOneByUserIdAndTitle(userId, title);
	}
	
	@Override
	public PhotoAlbum getById(Integer id) {
		return repository.findOne(id);
	}
	
	@Override
	public Integer countByUser(Integer userId) {
		return repository.countByUserId(userId);
	}
	
	@Override
	public PhotoAlbumResponse create(CreatePhotoAlbumRequest request) {
		PhotoAlbum photoAlbum = new PhotoAlbum();
		photoAlbum.setUserId(request.getUserId());
		photoAlbum.setTitle(request.getTitle());
		photoAlbum.setSlug(request.getSlug());
		photoAlbum.setDefaultPhoto(request.getDefaultPhoto());
		photoAlbum.setCreatedAt(new Date());
		photoAlbum.setUpdatedAt(new Date());
		photoAlbum = repository.save(photoAlbum);
		return getPhotoAlbumResponse(photoAlbum);
	}
	
	@Override
	public PhotoAlbumResponse getPhotoAlbumResponse(PhotoAlbum photoAlbum) {
		if (photoAlbum != null) {
			PhotoAlbumResponse response = new PhotoAlbumResponse();
			response.setId(photoAlbum.getId());
			response.setTitle(photoAlbum.getTitle());
			response.setOwner(userService.getUserInfoResponse(userService.getById(photoAlbum.getUserId())));
			response.setDefaultPhoto(photoAlbum.getDefaultPhoto());
			response.setCreatedAt(CommonUtils.getDuration(photoAlbum.getCreatedAt().getTime()));
			response.setUpdatedAt(CommonUtils.getDuration(photoAlbum.getUpdatedAt().getTime()));
			return response;
		} else {
			return null;
		}
	}
}
