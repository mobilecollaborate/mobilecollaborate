package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.entity.Newsfeed;
import com.diarium.collaborate.model.request.CreateNewsfeedRequest;
import com.diarium.collaborate.model.request.EditNewsfeedRequest;
import com.diarium.collaborate.model.request.ShareNewsfeedRequest;
import com.diarium.collaborate.model.response.NewsfeedInfoResponse;
import com.diarium.collaborate.model.response.NewsfeedResponse;

public interface NewsfeedService {

	List<Newsfeed> getForDashboard(Integer userId, Integer limit, Integer offset);
	List<Newsfeed> getForMyTimeline(Integer userId, Integer limit, Integer offset);
	List<Newsfeed> getForFriendsTimeline(Integer ownerId, Integer userId, Integer limit, Integer offset);
	List<Newsfeed> getForFollowingsTimeline(Integer ownerId, Integer userId, Integer limit, Integer offset);
	List<Newsfeed> getForOthersTimeline(Integer ownerId, Integer userId, Integer limit, Integer offset);
	List<Newsfeed> getForPage(Integer pageId, Integer userId, Integer limit, Integer offset);
	List<Newsfeed> getForCommunity(Integer communityId, Integer userId, Integer limit, Integer offset);
	Newsfeed getById(Integer id);
	
	NewsfeedResponse create(CreateNewsfeedRequest request) throws WebCrawlingFailedException;
	NewsfeedResponse edit(Newsfeed originalNewsfeed, EditNewsfeedRequest request) throws WebCrawlingFailedException;
	void delete(Newsfeed newsfeed) throws FileDeletionFailedException;
	NewsfeedResponse share(Newsfeed originalNewsfeed, ShareNewsfeedRequest request);
	
	NewsfeedInfoResponse getNewsfeedInfoResponse(Newsfeed newsfeed);
	NewsfeedResponse getNewsfeedResponse(Newsfeed newsfeed, Integer userId);
	
	void deleteEmbeddedData(Newsfeed newsfeed) throws FileDeletionFailedException;
}
