package com.diarium.collaborate.service;

import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.entity.BirthdayCard;
import com.diarium.collaborate.model.request.CreateBirthdayCardRequest;
import com.diarium.collaborate.model.response.NewsfeedResponse;

public interface BirthdayCardService {
	
	BirthdayCard getOne(Integer userId, Integer toUserId);
	BirthdayCard getByNewsfeed(Integer newsfeedId);

	NewsfeedResponse create(CreateBirthdayCardRequest request) throws WebCrawlingFailedException;
	void delete(Integer id);
}
