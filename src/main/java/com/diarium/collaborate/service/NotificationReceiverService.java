package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.NotificationReceiver;

public interface NotificationReceiverService {

	List<NotificationReceiver> getByType(String type, Integer typeId);
	NotificationReceiver getByUserAndType(Integer userId, String type, Integer typeId);
	
	void create(Integer userId, String type, Integer typeId);
	void delete(NotificationReceiver receiver);
}
