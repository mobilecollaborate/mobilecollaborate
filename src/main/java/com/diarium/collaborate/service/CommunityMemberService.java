package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.CommunityMember;
import com.diarium.collaborate.model.response.CommunityMemberResponse;

public interface CommunityMemberService {

	List<CommunityMember> getByCommunity(Integer communityId);

	CommunityMemberResponse getCommunityMemberResponse(CommunityMember communityMember);
}
