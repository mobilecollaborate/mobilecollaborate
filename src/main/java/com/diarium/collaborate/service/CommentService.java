package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.model.entity.Comment;
import com.diarium.collaborate.model.request.AddCommentRequest;
import com.diarium.collaborate.model.request.EditCommentRequest;
import com.diarium.collaborate.model.response.CommentResponse;

public interface CommentService {

	List<Comment> getByType(String type, Integer typeId);
	List<Comment> getByType(String type, Integer typeId, Integer limit, Integer offset);
	Comment getById(Integer id);

	Integer countByType(String type, Integer typeId);
	
	CommentResponse create(AddCommentRequest request);
	CommentResponse edit(Comment originalComment, EditCommentRequest request);
	void delete(Comment comment) throws FileDeletionFailedException;
	
	CommentResponse getCommentResponse(Comment comment, Integer userId);
	
	void deleteEmbeddedData(Comment comment) throws FileDeletionFailedException;
}
