package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.exception.FileDeletionFailedException;
import com.diarium.collaborate.model.entity.Photo;
import com.diarium.collaborate.model.request.CreatePhotoRequest;
import com.diarium.collaborate.model.response.PhotoResponse;

public interface PhotoService {

	List<Photo> getByUser(Integer userId, Integer limit, Integer offset);
	List<Photo> getByPost(Integer postId);
	Photo getById(Integer id);
	
	void create(CreatePhotoRequest request);
	Integer countByUser(Integer userId);
	
	void delete(Photo photo) throws FileDeletionFailedException;
	
	PhotoResponse getPhotoResponse(Photo photo);
}
