package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Hashtag;

public interface HashtagService {

	List<Hashtag> getTopTen();
	
	Hashtag add(String hash);
}
