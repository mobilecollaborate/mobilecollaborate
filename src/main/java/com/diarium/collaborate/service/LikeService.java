package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Like;
import com.diarium.collaborate.model.request.EditExpressionRequest;
import com.diarium.collaborate.model.request.GiveExpressionRequest;
import com.diarium.collaborate.model.response.LikeResponse;

public interface LikeService {

	List<Like> getByType(String type, Integer typeId);
	List<Like> getByType(String type, Integer typeId, Integer limit, Integer offset);
	Like getByUserAndType(Integer userId, String type, Integer typeId);
	Like getById(Integer id);

	Integer countByType(String type, Integer typeId);
	Integer countByTypeAndEmoji(String type, Integer typeId, Integer emoji);
	
	LikeResponse create(GiveExpressionRequest request);
	LikeResponse edit(Like originalLike, EditExpressionRequest request);
	void delete(Like like);
	
	LikeResponse getLikeResponse(Like like);
}
