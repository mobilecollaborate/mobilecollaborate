package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Expression;

public interface ExpressionService {

	List<Expression> getAllExpressions();
}
