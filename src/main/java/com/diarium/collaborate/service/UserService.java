package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.User;
import com.diarium.collaborate.model.response.UserInfoResponse;
import com.diarium.collaborate.model.response.UserProfileResponse;

public interface UserService {

	List<User> getByName(String name, Integer limit, Integer offset);
	List<User> getFriendsByName(Integer userId, String name);
	List<User> getFollowingsByName(Integer userId, String name);
	List<User> getFollowersByName(Integer userId, String name);
	List<User> getFriendsWhoHaveBirthdayToday(Integer userId, Integer limit, Integer offset);
	User getById(Integer id);
	
	UserInfoResponse getUserInfoResponse(User user);
	UserProfileResponse getUserProfileResponse(User user);
}
