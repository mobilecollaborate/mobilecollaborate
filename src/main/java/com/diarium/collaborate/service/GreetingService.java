package com.diarium.collaborate.service;

import com.diarium.collaborate.model.response.GreetingResponse;

public interface GreetingService {

	GreetingResponse getGreetingResponse(String name, String timeZone);
}
