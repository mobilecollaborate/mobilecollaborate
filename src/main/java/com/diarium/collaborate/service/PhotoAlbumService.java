package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.PhotoAlbum;
import com.diarium.collaborate.model.request.CreatePhotoAlbumRequest;
import com.diarium.collaborate.model.response.PhotoAlbumResponse;

public interface PhotoAlbumService {

	List<PhotoAlbum> getByUser(Integer userId, Integer limit, Integer offset);
	PhotoAlbum getByUserAndTitle(Integer userId, String title);
	PhotoAlbum getById(Integer id);
	
	Integer countByUser(Integer userId);
	
	PhotoAlbumResponse create(CreatePhotoAlbumRequest request);
	
	PhotoAlbumResponse getPhotoAlbumResponse(PhotoAlbum photoAlbum);
}
