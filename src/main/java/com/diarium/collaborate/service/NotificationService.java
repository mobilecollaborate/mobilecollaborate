package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Notification;
import com.diarium.collaborate.model.response.NotificationResponse;

public interface NotificationService {

	List<Notification> getByToUser(Integer userId, Integer limit, Integer offset);
	Notification getById(Integer id);
	
	Integer countByUser(Integer toUserId);
	
	void delete(Notification notification);
	
	NotificationResponse getNotificationResponse(Notification notification);
}
