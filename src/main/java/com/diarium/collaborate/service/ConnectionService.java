package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Connection;
import com.diarium.collaborate.model.request.CreateConnectionRequest;
import com.diarium.collaborate.model.response.FriendRequestResponse;
import com.diarium.collaborate.model.response.ConnectionResponse;

public interface ConnectionService {

	List<Connection> getFriends(Integer userId, Integer limit, Integer offset);
	List<Connection> getFriendRequests(Integer userId, Integer limit, Integer offset);
	List<Connection> getFollowings(Integer userId, Integer limit, Integer offset);
	List<Connection> getFollowers(Integer userId, Integer limit, Integer offset);
	List<Connection> getFriendConnections(Integer userId, Integer toUserId);
	Connection getFriendRequestConnection(Integer userId, Integer toUserId);
	Connection getFollowingConnection(Integer userId, Integer toUserId);
	
	Integer countFriendRequests(Integer userId);
	Integer countFriends(Integer userId);
	Integer countFollowings(Integer userId);
	Integer countFollowers(Integer userId);
	
	void create(CreateConnectionRequest request, Integer way, Integer confirmed);
	void edit(Connection connection, Integer way, Integer confirmed);
	void delete(Connection connection);

	ConnectionResponse getConnectionResponse(Connection connection, Integer userId);
	FriendRequestResponse getFriendRequestResponse(Connection connection);
	
	boolean isAlreadyFollower(Integer userId, Integer toUserId);
	boolean isAlreadyFriends(Integer userId, Integer toUserId);
}
