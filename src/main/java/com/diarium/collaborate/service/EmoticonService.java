package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Emoticon;

public interface EmoticonService {

	List<Emoticon> getAllEmoticons();
}
