package com.diarium.collaborate.service;

import java.util.List;

import com.diarium.collaborate.model.entity.Page;
import com.diarium.collaborate.model.response.PageInfoResponse;
import com.diarium.collaborate.model.response.PageResponse;

public interface PageService {

	List<Page> getByUser(Integer userId, Integer limit, Integer offset);
	Page getById(Integer id);
	
	PageInfoResponse getPageInfoResponse(Page page);
	PageResponse getPageResponse(Page page);
}
