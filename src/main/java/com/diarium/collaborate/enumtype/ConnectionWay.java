package com.diarium.collaborate.enumtype;

public enum ConnectionWay {

	FOLLOW(1), FRIEND(2);
	
	private Integer value;
	
	private ConnectionWay(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
}
