package com.diarium.collaborate.enumtype;

public enum DayTime {

	MORNING(3, 11, "Good morning"), AFTERNOON(12, 17, "Good afternoon"), EVENING(18, 2, "Good evening");
	
	private Integer startHour;
	private Integer endHour;
	private String greeting;
	
	private DayTime(Integer startHour, Integer endHour, String greeting) {
		this.startHour = startHour;
		this.endHour = endHour;
		this.greeting = greeting;
	}

	public Integer getStartHour() {
		return startHour;
	}

	public Integer getEndHour() {
		return endHour;
	}

	public String getGreeting() {
		return greeting;
	}
}
