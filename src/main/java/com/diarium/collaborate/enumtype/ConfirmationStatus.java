package com.diarium.collaborate.enumtype;

public enum ConfirmationStatus {

	NOT_YET_CONFIRMED(0), CONFIRMED(1);
	
	private Integer value;
	
	private ConfirmationStatus(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
}
