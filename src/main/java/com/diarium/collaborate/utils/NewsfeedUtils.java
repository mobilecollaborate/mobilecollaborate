package com.diarium.collaborate.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.bind.DatatypeConverter;

import com.diarium.collaborate.exception.WebCrawlingFailedException;
import com.diarium.collaborate.model.Link;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlMeta;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import de.ailis.pherialize.MixedArray;
import de.ailis.pherialize.Pherialize;

public class NewsfeedUtils {

	public static String encodeTypeContents(String[] typeContents) {
		if (typeContents != null) {
			if (typeContents.length > 0) {
				return new String(DatatypeConverter.printBase64Binary(Pherialize.serialize(typeContents).getBytes()));
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static List<String> decodeTypeContents(String encodedTypeContent, String type, String baseUrl) {
		if (encodedTypeContent != null) {
			if (!encodedTypeContent.isEmpty()) {
				encodedTypeContent = new String(DatatypeConverter.parseBase64Binary(encodedTypeContent));
				MixedArray list = Pherialize.unserialize(encodedTypeContent).toArray();
				List<String> typeContents = new ArrayList<String>();
				
				for (int i = 0; i < list.size(); i++) {
					String url = "";
					
					if (type != null && baseUrl != null) {
						if (type.equals("image")) {
							url = baseUrl;
						}
					}
					
					typeContents.add(url + list.get(i));
				}
				
				return typeContents;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static String encodeLink(String uri) throws WebCrawlingFailedException {
		if (uri != null) {
			if (!uri.isEmpty()) {
				try {
					java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
					
					@SuppressWarnings("resource")
					WebClient client = new WebClient(BrowserVersion.CHROME);
					client.getOptions().setUseInsecureSSL(true);
					client.getOptions().setThrowExceptionOnScriptError(false);
					client.getOptions().setThrowExceptionOnFailingStatusCode(false);
					
					HtmlPage page = client.getPage(uri);
					HtmlMeta desc = page.getFirstByXPath("//meta[@property='og:description']");
					HtmlMeta img = page.getFirstByXPath("//meta[@property='og:image']");
					
					Map<String, String> link = new LinkedHashMap<String, String>();
					link.put("link", uri);
					link.put("title", page.getTitleText());
					link.put("description", desc.getContentAttribute());
					link.put("image", img.getContentAttribute());
					return new String(DatatypeConverter.printBase64Binary(Pherialize.serialize(link).getBytes()));
				} catch (Exception e) {
					throw new WebCrawlingFailedException();
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static Link decodeLink(String encodedLink) {
		try {
			if (encodedLink != null) {
				if (!encodedLink.isEmpty()) {
					encodedLink = new String(DatatypeConverter.parseBase64Binary(encodedLink));
					MixedArray list = Pherialize.unserialize(encodedLink).toArray();
					Link link = new Link();
					link.setLink(list.getString("link"));
					link.setTitle(list.getString("title"));
					link.setDescription(list.getString("description"));
					link.setImage(list.getString("image"));
					return link;
				} else {
					return null;
				} 
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String encodeTags(String[] tags) {
		if (tags != null) {
			if (tags.length > 0) {
				return new String(DatatypeConverter.printBase64Binary(Pherialize.serialize(tags).getBytes()));
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static List<String> decodeTags(String encodedTags) {
		if (encodedTags != null) {
			if (!encodedTags.isEmpty()) {
				encodedTags = new String(DatatypeConverter.parseBase64Binary(encodedTags));
				MixedArray list = Pherialize.unserialize(encodedTags).toArray();
				List<String> tags = new ArrayList<String>();

				for (int i = 0; i < list.size(); i++) {
					tags.add(list.getString(i));
				}

				return tags;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
