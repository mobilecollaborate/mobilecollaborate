package com.diarium.collaborate.utils;

import java.time.Duration;
import java.time.Instant;

public class CommonUtils {

	public final static String getCollaborateBaseUrl() {
		return "https://diarium.telkom.co.id/";
	}
	
	public static String getDuration(Long timestamp) {
		if (timestamp != null) {
			String string;
			Instant now = Instant.now();
			Instant time = Instant.ofEpochMilli(timestamp);
			Duration duration = Duration.between(time, now);
			
			if (duration.getSeconds() < 60) {
				string = duration.getSeconds() + " seconds ago";
			} else if (duration.toMinutes() < 60) {
				string = duration.toMinutes() + " minutes ago";
			} else if (duration.toHours() < 24) {
				string = duration.toHours() + " hours ago";
			} else if (duration.toDays() < 30) {
				string = duration.toDays() + " days ago";
			} else if (duration.toDays() < 365) {
				string = Long.toString((duration.toDays()/30)) + " months ago";
			} else {
				string = Long.toString((duration.toDays()/365)) + " years ago";
			}
			
			return string;
		} else {
			return null;
		}
	}
}
