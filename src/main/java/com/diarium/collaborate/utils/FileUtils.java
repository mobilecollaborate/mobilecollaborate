package com.diarium.collaborate.utils;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.xmlpull.v1.XmlPullParserException;

import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidObjectPrefixException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.NoResponseException;
import io.minio.errors.RegionConflictException;
import io.minio.policy.PolicyType;

public class FileUtils {
	
	private static MinioClient minioClient;
	
	public static String getMinioBucketName() {
		return "webapp";
	}

	public static MinioClient getMinioClient() throws InvalidEndpointException, InvalidPortException, InvalidKeyException, InvalidBucketNameException, NoSuchAlgorithmException, InsufficientDataException, NoResponseException, ErrorResponseException, InternalException, IOException, XmlPullParserException, RegionConflictException, InvalidObjectPrefixException {
		minioClient = new MinioClient("http://10.1.72.106:9000", "NUSZ5I60SPNIZDQ0FWD7", "f9drEc7LiU+MKwIWaDEW6qKf3wzCw3Jruy6HNwsl");
//		minioClient = new MinioClient("http://10.2.44.138:9000", "XHVROLT8NPBYCRNMEL3Q", "s4tVs80j1Jx24e9S6crRT3uu5lH5UkTC6/Z/Noty");
		
		if (!minioClient.bucketExists(getMinioBucketName())) {
			minioClient.makeBucket(getMinioBucketName());
			minioClient.setBucketPolicy(FileUtils.getMinioBucketName(), "", PolicyType.READ_WRITE);
		}
		
		return minioClient;
	}

	public static String encodeFilename(String filename) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(filename.getBytes());
		return DatatypeConverter.printHexBinary(md.digest());
	}

	public static boolean isImage(String extension) {
		if (extension.equals("jpg") || extension.equals("jpeg") || extension.equals("png") || extension.equals("gif")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isVideo(String extension) {
		if (extension.equals("mkv") || extension.equals("mp4") || extension.equals("3gp")) {
			return true;
		} else {
			return false;
		}
	}
}
