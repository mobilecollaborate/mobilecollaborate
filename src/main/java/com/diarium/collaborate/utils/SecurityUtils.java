package com.diarium.collaborate.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SecurityUtils {

	@SuppressWarnings("unchecked")
	private static Map<String, Object> parsingAuth() {
        ObjectMapper objectMapper = new ObjectMapper();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Map<String, Object> map = objectMapper.convertValue(authentication.getDetails(), Map.class);

        // create a token object to represent the token that is in use.
        Jwt jwt = JwtHelper.decode((String) map.get("tokenValue"));

        // jwt.getClaims() will return a JSON object of all the claims in your token
        // Convert claims JSON object into a Map so we can get the value of a field
        Map<String, Object> claims = new HashMap<>();
        try {
            claims = objectMapper.readValue(jwt.getClaims(), Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return claims;
    }

    public static String getUsernameFromPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public static String getNamaFromPrincipal() {
        return (String) parsingAuth().get("nama");
    }

    public static String getJabatanFromPrincipal() {
        return (String) parsingAuth().get("jabatan");
    }

    public static String getUnitFromPrincipal() {
        return (String) parsingAuth().get("unit");
    }
    
    public static Integer getCollaborateUserIdFromPrincipal() {
        return (Integer) parsingAuth().get("id_user");
    }
}
