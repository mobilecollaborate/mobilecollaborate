package com.diarium.collaborate;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private String serviceName = "mobilecollaborate";

    /*@Bean
    public OncePerRequestFilter authenticationTokenFilterBean() throws Exception {
        return new FilterIpAddress();
    }*/

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources",
                        "/configuration/security",
                        "/webjars/",
                        "/swagger-resources/configuration/ui",
                        "/swagger-ui.html",
                        "/*.html",
                        "/favicon.ico",
                        "//*.html",
                        "//*.css",
                        "//*.js"
                ).permitAll()
                .anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(serviceName);
    }
}
