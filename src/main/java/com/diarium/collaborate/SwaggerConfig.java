package com.diarium.collaborate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Lists;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket petApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.globalOperationParameters(
						Lists.newArrayList(new ParameterBuilder()
						.name("X-Authorization")
						.description("OAUTH2 Token")
						.modelRef(new ModelRef("string"))
						.parameterType("header")
						.required(true)
						.build()))
					.select()
					.apis(RequestHandlerSelectors.any())
					.apis(RequestHandlerSelectors.basePackage("com.diarium.collaborate.controller"))
					.paths(PathSelectors.any())
					.build()
					.apiInfo(apiInfo());
	}
	
	private ApiInfo apiInfo() {
		ApiInfo info = new ApiInfoBuilder()
			.title("Mobile-Collaborate")
			.description("Collaborate API")
			.contact(new Contact("PT. Swamedia Informatika", "http://swamedia.co.id/"
			, "john.doe@swamedia.co.id"))
			.version("v1")
			.termsOfServiceUrl("dummy.telkom.co.id")
			.license("Lisensi untuk Telkom Indonesia")
			.licenseUrl("http://www.telkom.co.id/")
			.build();
		return info;
	}
}
