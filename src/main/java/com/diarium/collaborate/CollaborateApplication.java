package com.diarium.collaborate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollaborateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollaborateApplication.class, args);
	}
}
