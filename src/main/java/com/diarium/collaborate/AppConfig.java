package com.diarium.collaborate;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.diarium.collaborate.controller", "com.diarium.collaborate.service"})
@EnableDiscoveryClient
public class AppConfig {

}
