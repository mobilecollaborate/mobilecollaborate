package com.diarium.collaborate.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
@Entity
@Table(name = "tb_posts")
public class Newsfeed implements EntityModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = JsonProperty.Access.AUTO)
	@Column
	private Integer id;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "to_user_id")
	private Integer toUserId;
	@Column(columnDefinition = "TEXT")
	private String text;
	@Column(name = "content_type", columnDefinition = "TEXT")
	private String contentType;
	@Column(name = "type_content", columnDefinition = "TEXT")
	private String typeContent;
	@Column(columnDefinition = "TEXT")
	private String type;
	@Column(name = "type_id")
	private Integer typeId;
	@Column(name = "community_id")
	private Integer communityId;
	@Column(name = "page_id")
	private Integer pageId;
	@Column(columnDefinition = "TEXT")
	private String link;
	@Column(columnDefinition = "TEXT")
	private String tags;
	@Column
	private Integer privacy;
	@Column
	private Integer shared;
	@Column(name = "shared_id")
	private Integer sharedId;
	@Column(name = "shared_from")
	private Integer sharedFrom;
	@Column(name = "shared_count")
	private Integer sharedCount;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;
	@Column
	private Integer edited;
	@Column(name = "video_path",columnDefinition = "TEXT")
	private String videoPath;
	@Column(name = "auto_like_id")
	private String autoLikeId;
	@Column(name = "file_path", columnDefinition = "TEXT")
	private String filePath;
	@Column(name = "file_path_name", columnDefinition = "TEXT")
	private String filePathName;
	@Column
	private Integer pinned;
	
	public Newsfeed() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getToUserId() {
		return toUserId;
	}

	public void setToUserId(Integer toUserId) {
		this.toUserId = toUserId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getTypeContent() {
		return typeContent;
	}

	public void setTypeContent(String typeContent) {
		this.typeContent = typeContent;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Integer communityId) {
		this.communityId = communityId;
	}

	public Integer getPageId() {
		return pageId;
	}

	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Integer getShared() {
		return shared;
	}

	public void setShared(Integer shared) {
		this.shared = shared;
	}

	public Integer getSharedId() {
		return sharedId;
	}

	public void setSharedId(Integer sharedId) {
		this.sharedId = sharedId;
	}

	public Integer getSharedFrom() {
		return sharedFrom;
	}

	public void setSharedFrom(Integer sharedFrom) {
		this.sharedFrom = sharedFrom;
	}

	public Integer getSharedCount() {
		return sharedCount;
	}

	public void setSharedCount(Integer sharedCount) {
		this.sharedCount = sharedCount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getEdited() {
		return edited;
	}

	public void setEdited(Integer edited) {
		this.edited = edited;
	}

	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public String getAutoLikeId() {
		return autoLikeId;
	}

	public void setAutoLikeId(String autoLikeId) {
		this.autoLikeId = autoLikeId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePathName() {
		return filePathName;
	}

	public void setFilePathName(String filePathName) {
		this.filePathName = filePathName;
	}

	public Integer getPinned() {
		return pinned;
	}

	public void setPinned(Integer pinned) {
		this.pinned = pinned;
	}
}
