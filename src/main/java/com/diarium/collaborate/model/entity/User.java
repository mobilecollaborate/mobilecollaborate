package com.diarium.collaborate.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
@Entity
@Table(name = "tb_users")
public class User implements EntityModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = JsonProperty.Access.AUTO)
	@Column
	private Integer id;
	@Column
	private String fullname;
	@Column
	private String username;
	@Column(name = "email_address")
	private String emailAddress;
	@Column
	private String password;
	@Column
	private String genre;
	@Column(columnDefinition = "TEXT")
	private String bio;
	@Column(name = "profile_details", columnDefinition = "TEXT")
	private String profileDetails;
	@Column(name = "privacy_info", columnDefinition = "TEXT")
	private String privacyInfo;
	@Column(name = "design_info", columnDefinition = "TEXT")
	private String designInfo;
	@Column(columnDefinition = "TEXT")
	private String cover;
	@Column
	private String country;
	@Column(name = "fully_started")
	private Integer fullyStarted;
	@Column(columnDefinition = "TEXT")
	private String avatar;
	@Column
	private String auth;
	@Column(name = "auth_id")
	private String authId;
	@Column
	private Integer verified;
	@Column
	private Integer admin;
	@Column
	private Integer active;
	@Column
	private Integer activated;
	@Column
	private String hash;
	@Column(name = "remember_token")
	private String rememberToken;
	@Column(name = "last_active_time")
	private Integer lastActiveTime;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;
	@Column(columnDefinition = "TEXT")
	private String city;
	@Column(name = "original_cover", columnDefinition = "TEXT")
	private String originalCover;
	@Column(name = "online_status")
	private Integer onlineStatus;
	@Column
	private Integer banned;
	@Column(name = "birth_day")
	private Integer birthDay;
	@Column(name = "birth_month")
	private Integer birthMonth;
	@Column(name = "birth_year")
	private Integer birthYear;
	@Column
	private Integer ldap;
	@Column
	private String jabatan;
	@Column
	private String band;
	@Column
	private String kota;
	@Column
	private String loker;
	@Column
	private String divisi;
	@Column
	private String param1;
	@Column
	private String unit;
	@Column
	private String param2;
	@Column
	private String religion;
	@Column(name = "marital_status")
	private String maritalStatus;
	@Column(name = "employee_group")
	private String employeeGroup;
	@Column(name = "employee_subgroup")
	private String employeeSubgroup;
	@Column(name = "ethnic_group")
	private String ethnicGroup;
	@Column(name = "blood_type")
	private String bloodType;
	
	public User() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getProfileDetails() {
		return profileDetails;
	}

	public void setProfileDetails(String profileDetails) {
		this.profileDetails = profileDetails;
	}

	public String getPrivacyInfo() {
		return privacyInfo;
	}

	public void setPrivacyInfo(String privacyInfo) {
		this.privacyInfo = privacyInfo;
	}

	public String getDesignInfo() {
		return designInfo;
	}

	public void setDesignInfo(String designInfo) {
		this.designInfo = designInfo;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getFullyStarted() {
		return fullyStarted;
	}

	public void setFullyStarted(Integer fullyStarted) {
		this.fullyStarted = fullyStarted;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getAuthId() {
		return authId;
	}

	public void setAuthId(String authId) {
		this.authId = authId;
	}

	public Integer getVerified() {
		return verified;
	}

	public void setVerified(Integer verified) {
		this.verified = verified;
	}

	public Integer getAdmin() {
		return admin;
	}

	public void setAdmin(Integer admin) {
		this.admin = admin;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getActivated() {
		return activated;
	}

	public void setActivated(Integer activated) {
		this.activated = activated;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getRememberToken() {
		return rememberToken;
	}

	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}

	public Integer getLastActiveTime() {
		return lastActiveTime;
	}

	public void setLastActiveTime(Integer lastActiveTime) {
		this.lastActiveTime = lastActiveTime;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getOriginalCover() {
		return originalCover;
	}

	public void setOriginalCover(String originalCover) {
		this.originalCover = originalCover;
	}

	public Integer getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(Integer onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public Integer getBanned() {
		return banned;
	}

	public void setBanned(Integer banned) {
		this.banned = banned;
	}

	public Integer getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Integer birthDay) {
		this.birthDay = birthDay;
	}

	public Integer getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(Integer birthMonth) {
		this.birthMonth = birthMonth;
	}

	public Integer getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}

	public Integer getLdap() {
		return ldap;
	}

	public void setLdap(Integer idap) {
		this.ldap = idap;
	}

	public String getJabatan() {
		return jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}

	public String getBand() {
		return band;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public String getKota() {
		return kota;
	}

	public void setKota(String kota) {
		this.kota = kota;
	}

	public String getLoker() {
		return loker;
	}

	public void setLoker(String loker) {
		this.loker = loker;
	}

	public String getDivisi() {
		return divisi;
	}

	public void setDivisi(String divisi) {
		this.divisi = divisi;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getEmployeeGroup() {
		return employeeGroup;
	}

	public void setEmployeeGroup(String employeeGroup) {
		this.employeeGroup = employeeGroup;
	}

	public String getEmployeeSubgroup() {
		return employeeSubgroup;
	}

	public void setEmployeeSubgroup(String employeeSubgroup) {
		this.employeeSubgroup = employeeSubgroup;
	}

	public String getEthnicGroup() {
		return ethnicGroup;
	}

	public void setEthnicGroup(String ethnicGroup) {
		this.ethnicGroup = ethnicGroup;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}
}
