package com.diarium.collaborate.model.request;

public class EditCommentRequest {

	private Integer id;
	private String text;
	private String imgPath;
	
	public EditCommentRequest() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
}
