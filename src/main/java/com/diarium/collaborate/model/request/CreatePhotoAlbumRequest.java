package com.diarium.collaborate.model.request;

@SuppressWarnings("serial")
public class CreatePhotoAlbumRequest implements RequestModel {

	private Integer userId;
	private String title;
	private String slug;
	private String defaultPhoto;
	
	public CreatePhotoAlbumRequest() {
		
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDefaultPhoto() {
		return defaultPhoto;
	}

	public void setDefaultPhoto(String defaultPhoto) {
		this.defaultPhoto = defaultPhoto;
	}
}
