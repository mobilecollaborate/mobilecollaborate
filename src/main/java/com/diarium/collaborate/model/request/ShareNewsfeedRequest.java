package com.diarium.collaborate.model.request;

public class ShareNewsfeedRequest {

	private Integer userId;
	private Integer toUserId;
	private String text;
	private String type;
	private Integer typeId;
	private Integer communityId;
	private Integer pageId;
	private String[] tags;
	private Integer privacy;
	private Integer shared;
	private Integer sharedId;
	private Integer sharedFrom;
//	private String autoLikeId;
//	private Integer pinned;
	
	public ShareNewsfeedRequest() {
		
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getToUserId() {
		return toUserId;
	}

	public void setToUserId(Integer toUserId) {
		this.toUserId = toUserId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getCommunityId() {
		return communityId;
	}

	public void setCommunityId(Integer communityId) {
		this.communityId = communityId;
	}

	public Integer getPageId() {
		return pageId;
	}

	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Integer getShared() {
		return shared;
	}

	public void setShared(Integer shared) {
		this.shared = shared;
	}

	public Integer getSharedId() {
		return sharedId;
	}

	public void setSharedId(Integer sharedId) {
		this.sharedId = sharedId;
	}

	public Integer getSharedFrom() {
		return sharedFrom;
	}

	public void setSharedFrom(Integer sharedFrom) {
		this.sharedFrom = sharedFrom;
	}
}
