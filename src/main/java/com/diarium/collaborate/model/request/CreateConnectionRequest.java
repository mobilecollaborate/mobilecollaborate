package com.diarium.collaborate.model.request;

public class CreateConnectionRequest {

	private Integer userId;
	private Integer toUserId;
	
	public CreateConnectionRequest() {
		
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getToUserId() {
		return toUserId;
	}

	public void setToUserId(Integer toUserId) {
		this.toUserId = toUserId;
	}
}
