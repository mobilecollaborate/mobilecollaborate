package com.diarium.collaborate.model.request;

public class EditExpressionRequest {

	private Integer userId;
	private String type;
	private Integer typeId;
	private Integer emoji;
	
	public EditExpressionRequest() {
		
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getEmoji() {
		return emoji;
	}

	public void setEmoji(Integer emoji) {
		this.emoji = emoji;
	}
}
