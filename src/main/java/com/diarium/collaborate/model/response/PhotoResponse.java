package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class PhotoResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse owner;
	private String slug;
	private String path;
	private String createdAt;
	private String updatedAt;
	private PageInfoResponse page;
	private Integer privacy;
	private NewsfeedInfoResponse newsfeed;

	public PhotoResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getOwner() {
		return owner;
	}

	public void setOwner(UserInfoResponse owner) {
		this.owner = owner;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public PageInfoResponse getPage() {
		return page;
	}

	public void setPage(PageInfoResponse page) {
		this.page = page;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public NewsfeedInfoResponse getNewsfeed() {
		return newsfeed;
	}

	public void setNewsfeed(NewsfeedInfoResponse newsfeed) {
		this.newsfeed = newsfeed;
	}
}
