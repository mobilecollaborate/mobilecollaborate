package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class GreetingResponse implements ResponseModel {

	private String greeting;
	private String timeZone;
	private String localTime;
	
	public GreetingResponse() {
		
	}

	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLocalTime() {
		return localTime;
	}

	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}
}
