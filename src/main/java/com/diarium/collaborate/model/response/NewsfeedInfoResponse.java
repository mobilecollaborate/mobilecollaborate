package com.diarium.collaborate.model.response;

import java.util.List;

import com.diarium.collaborate.model.Link;

@SuppressWarnings("serial")
public class NewsfeedInfoResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse owner;
	private UserInfoResponse receiver;
	private String text;
	private String contentType;
	private List<String> typeContent;
	private CommunityInfoResponse community;
	private PageInfoResponse page;
	private Link link;
	private List<String> tags;
	private Integer privacy;
	private Integer shared;
	private UserInfoResponse sharedFrom;
	private Integer sharedCount;
	private String createdAt;
	private String updatedAt;
	private Integer edited;
	private String videoPath;
	private String autoLikeId;
	private String filePath;
	private String filePathName;
	
	private Integer commentsCount;
	private Integer likesCount;
	
	public NewsfeedInfoResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getOwner() {
		return owner;
	}

	public void setOwner(UserInfoResponse owner) {
		this.owner = owner;
	}

	public UserInfoResponse getReceiver() {
		return receiver;
	}

	public void setReceiver(UserInfoResponse receiver) {
		this.receiver = receiver;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public List<String> getTypeContent() {
		return typeContent;
	}

	public void setTypeContent(List<String> typeContent) {
		this.typeContent = typeContent;
	}

	public CommunityInfoResponse getCommunity() {
		return community;
	}

	public void setCommunity(CommunityInfoResponse community) {
		this.community = community;
	}

	public PageInfoResponse getPage() {
		return page;
	}

	public void setPage(PageInfoResponse page) {
		this.page = page;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Integer getShared() {
		return shared;
	}

	public void setShared(Integer shared) {
		this.shared = shared;
	}

	public UserInfoResponse getSharedFrom() {
		return sharedFrom;
	}

	public void setSharedFrom(UserInfoResponse sharedFrom) {
		this.sharedFrom = sharedFrom;
	}

	public Integer getSharedCount() {
		return sharedCount;
	}

	public void setSharedCount(Integer sharedCount) {
		this.sharedCount = sharedCount;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getEdited() {
		return edited;
	}

	public void setEdited(Integer edited) {
		this.edited = edited;
	}

	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public String getAutoLikeId() {
		return autoLikeId;
	}

	public void setAutoLikeId(String autoLikeId) {
		this.autoLikeId = autoLikeId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePathName() {
		return filePathName;
	}

	public void setFilePathName(String filePathName) {
		this.filePathName = filePathName;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	public Integer getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(Integer likesCount) {
		this.likesCount = likesCount;
	}
}
