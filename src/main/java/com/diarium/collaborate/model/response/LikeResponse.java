package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class LikeResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse owner;
	private String createdAt;
	private String updatedAt;
	private Integer emoji;
	
	public LikeResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getOwner() {
		return owner;
	}

	public void setOwner(UserInfoResponse owner) {
		this.owner = owner;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getEmoji() {
		return emoji;
	}

	public void setEmoji(Integer emoji) {
		this.emoji = emoji;
	}
}
