package com.diarium.collaborate.model.response;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class CommentResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse owner;
	private String text;
	private String imgPath;
	private String createdAt;
	private String updatedAt;
	
	private Integer likesCount;
	private List<LikeResponse> likes;
	private Integer givenExpression;
	private boolean	alreadyLike;

	public CommentResponse() {
		likes = new ArrayList<LikeResponse>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getOwner() {
		return owner;
	}

	public void setOwner(UserInfoResponse owner) {
		this.owner = owner;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(Integer likesCount) {
		this.likesCount = likesCount;
	}

	public List<LikeResponse> getLikes() {
		return likes;
	}

	public void setLikes(List<LikeResponse> likes) {
		this.likes = likes;
	}

	public Integer getGivenExpression() {
		return givenExpression;
	}

	public void setGivenExpression(Integer givenExpression) {
		this.givenExpression = givenExpression;
	}
	
	public boolean isAlreadyLike() {
		return alreadyLike;
	}

	public void setAlreadyLike(boolean alreadyLike) {
		this.alreadyLike = alreadyLike;
	}
}
