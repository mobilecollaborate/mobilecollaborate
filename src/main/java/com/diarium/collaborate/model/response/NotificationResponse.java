package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class NotificationResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse owner;
	private UserInfoResponse receiver;
	private String title;
	private String content;
	private String data;
	private Integer seen;
	private String createdAt;
	private String updatedAt;
	
	public NotificationResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getOwner() {
		return owner;
	}

	public void setOwner(UserInfoResponse owner) {
		this.owner = owner;
	}

	public UserInfoResponse getReceiver() {
		return receiver;
	}

	public void setToUserId(UserInfoResponse receiver) {
		this.receiver = receiver;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Integer getSeen() {
		return seen;
	}

	public void setSeen(Integer seen) {
		this.seen = seen;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
