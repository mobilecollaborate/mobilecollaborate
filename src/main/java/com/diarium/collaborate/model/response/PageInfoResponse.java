package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class PageInfoResponse implements ResponseModel {

	private Integer id;
	private String title;
	private String slug;
	private String logo;
	
	public PageInfoResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
}
