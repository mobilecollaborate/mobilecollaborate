package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class FilenameResponse implements ResponseModel {

	private String path;
	private String filename;
	
	public FilenameResponse() {
		
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
