package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class ConnectionResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse connection;
	private String createdAt;
	private String updatedAt;
	
	public ConnectionResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getConnection() {
		return connection;
	}

	public void setConnection(UserInfoResponse connection) {
		this.connection = connection;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
