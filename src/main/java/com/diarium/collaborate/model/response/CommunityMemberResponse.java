package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class CommunityMemberResponse implements ResponseModel {

	private Integer id;
	private UserInfoResponse user;
	private String createdAt;
	private String updatedAt;
	
	public CommunityMemberResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserInfoResponse getUser() {
		return user;
	}

	public void setUser(UserInfoResponse user) {
		this.user = user;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
}
