package com.diarium.collaborate.model.response;

@SuppressWarnings("serial")
public class UserInfoResponse implements ResponseModel {

	private Integer id;
	private String fullname;
	private String username;
	private String avatar;
	private Integer lastActiveTime;
	private Integer onlineStatus;
	private String jabatan;
	private Integer age;

	public UserInfoResponse() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getLastActiveTime() {
		return lastActiveTime;
	}

	public void setLastActiveTime(Integer lastActiveTime) {
		this.lastActiveTime = lastActiveTime;
	}

	public Integer getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(Integer onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public String getJabatan() {
		return jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
