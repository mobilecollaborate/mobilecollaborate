package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class DataAlreadyExistException extends Exception {

	public DataAlreadyExistException() {
		super("Data sudah ada");
	}
	
	public String getResponseCode() {
		return "609";
	}
}
