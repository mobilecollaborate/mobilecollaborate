package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class FileDeletionFailedException extends Exception {

	public FileDeletionFailedException() {
		super("File gagal dihapus");
	}
	
	public String getResponseCode() {
		return "803";
	}
}
