package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class EmptyFileUploadedException extends Exception {

	public EmptyFileUploadedException() {
		super("File yang diunggah");
	}
	
	public String getResponseCode() {
		return "800";
	}
}
