package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class UnsupportedUploadTypeException extends Exception {

	public UnsupportedUploadTypeException() {
		super("Tipe upload file tidak didukung");
	}
	
	public String getResponseCode() {
		return "802";
	}
}
