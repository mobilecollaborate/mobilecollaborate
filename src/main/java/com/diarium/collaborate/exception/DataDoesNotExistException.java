package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class DataDoesNotExistException extends Exception {

	public DataDoesNotExistException() {
		super("Tidak ada data");
	}
	
	public String getResponseCode() {
		return "600";
	}
}
