package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class UnsupportedFileExtensionException extends Exception {

	public UnsupportedFileExtensionException() {
		super("Ekstensi file tidak didukung");
	}
	
	public String getResponseCode() {
		return "801";
	}
}
