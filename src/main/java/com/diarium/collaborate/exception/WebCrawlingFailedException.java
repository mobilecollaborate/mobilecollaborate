package com.diarium.collaborate.exception;

@SuppressWarnings("serial")
public class WebCrawlingFailedException extends Exception {

	public WebCrawlingFailedException() {
		super("Web crawling gagal");
	}
	
	public String getResponseCode() {
		return "900";
	}
}
