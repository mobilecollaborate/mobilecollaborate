package com.diarium.collaborate.response;

public class NoDataResponse extends ResponseTemplate {

	public NoDataResponse(String status, String message, Long time) {
		super(status, message, time);
	}
}
