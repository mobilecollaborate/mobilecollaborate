package com.diarium.collaborate.response;

public class SingleDataResponse<T extends Object> extends ResponseTemplate {

	private T data;
	
	public SingleDataResponse(String status, String message, T data, Long time) {
		super(status, message, time);
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
