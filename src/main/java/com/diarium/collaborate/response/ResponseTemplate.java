package com.diarium.collaborate.response;

import java.math.BigDecimal;

public abstract class ResponseTemplate {

	private String status;
	private String message;
	private String executionTime;
	
	public ResponseTemplate(String status, String message, Long time) {
		this.status = status;
		this.message = message;
		this.executionTime = BigDecimal.valueOf(((System.nanoTime() - time.doubleValue())/1000000000)) + " second(s)";
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
}
