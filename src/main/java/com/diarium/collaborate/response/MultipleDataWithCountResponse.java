package com.diarium.collaborate.response;

import java.util.List;

import com.diarium.collaborate.model.response.ResponseModel;

public class MultipleDataWithCountResponse<T extends ResponseModel> extends MultipleDataWithoutCountResponse<T> {

	private Integer count;

	public MultipleDataWithCountResponse(String status, String message, List<T> data, Integer count, Long time) {
		super(status, message, data, time);
		this.count = count;
	}
	
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
