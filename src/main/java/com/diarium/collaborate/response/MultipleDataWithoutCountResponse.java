package com.diarium.collaborate.response;

import java.util.List;

import com.diarium.collaborate.model.response.ResponseModel;

public class MultipleDataWithoutCountResponse<T extends ResponseModel> extends ResponseTemplate {

	private List<T> data;

	public MultipleDataWithoutCountResponse(String status, String message, List<T> data, Long time) {
		super(status, message, time);
		this.data = data;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
