package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{

	@Query(value = "select * from smartbook.tb_users as tbu " + 
			"	where tbu.fullname like :name " + 
//	"-- final touch  " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<User> findByName(@Param("name")String name, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "select * from ( " + 
//			"-- requested friend  " + 
					"select distinct tbu.* from smartbook.tb_users as tbu inner join smartbook.tb_connections as tbc " + 
					"	on tbu.id = tbc.to_user_id " + 
					"where  " + 
					"	( tbc.user_id = :userId ) " + 
					"	and tbc.confirmed = 1 " + 
					"	and tbc.way = 2 " + 
					"	and tbu.fullname like :name " + 
					"union  " + 
//			"-- accepted friend  " + 
					"select distinct tbu.* from smartbook.tb_users as tbu inner join smartbook.tb_connections as tbc " + 
					"	on tbu.id = tbc.user_id " + 
					"where  " + 
					"	( tbc.to_user_id = :userId ) " + 
					"	and tbc.confirmed = 1 " + 
					"	and tbc.way = 2 " + 
					"	and tbu.fullname like :name ) as u " + 
//			"-- final touch  " + 
					"order by created_at desc;", nativeQuery = true)
			List<User> findFriendsByName(@Param("userId") Integer userId, @Param("name")String name);
	@Query(value = "select * from ( " + 
//	"-- following  " + 
			"select distinct tbu.* from smartbook.tb_users as tbu inner join smartbook.tb_connections as tbc " + 
			"	on tbu.id = tbc.to_user_id " + 
			"where  " + 
			"	( tbc.user_id = :userId ) " + 
			"	and tbc.way = 1 " + 
			"	and tbu.fullname like :name " + 
			") as u " + 
//	"-- final touch  " + 
			"order by created_at desc;", nativeQuery = true)
	List<User> findFollowingsByName(@Param("userId") Integer userId, @Param("name")String name);
	@Query(value = "select * from ( " + 
//	"-- follower  " + 
			"select distinct tbu.* from smartbook.tb_users as tbu inner join smartbook.tb_connections as tbc " + 
			"	on tbu.id = tbc.to_user_id " + 
			"where  " + 
			"	( tbc.to_user_id = :userId ) " + 
			"	and tbc.way = 1 " + 
			"	and tbu.fullname like :name " + 
			") as u " + 
//	"-- final touch  " + 
			"order by created_at desc;", nativeQuery = true)
	List<User> findFollowersByName(@Param("userId") Integer userId, @Param("name")String name);
	@Query(value = "select * from ( " + 
			"select distinct tbu.* from smartbook.tb_users as tbu inner join smartbook.tb_connections as tbc " + 
			"	on tbu.id = tbc.user_id or tbu.id = tbc.to_user_id " + 
			"where " + 
			"	( tbc.user_id = :userId or tbc.to_user_id = :userId ) " + 
			"	and tbc.confirmed = 1 " + 
			"	and tbc.way = 2 " + 
			"	and tbu.birth_day = :birthDay " + 
			"	and tbu.birth_month = :birthMonth " + 
			") as u where u.id not in ( " + 
			"	select tbbc.to_user_id from smartbook.tb_birthday_cards as tbbc " + 
			"   where tbbc.user_id = :userId " + 
			"   	and tbbc.year = :year ) " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<User> findFriendsWhoBirthdayToday(@Param("userId") Integer userId, @Param("birthDay") Integer birthDay, @Param("birthMonth") Integer birthMonth, @Param("year") Integer year, @Param("offset") Integer offset, @Param("limit") Integer limit);
	List<User> findByFullnameOrderByFullnameAsc(String fullname);
	
	User findOneByUsername(String username);
}
