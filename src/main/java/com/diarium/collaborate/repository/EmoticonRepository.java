package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Emoticon;

@Repository
public interface EmoticonRepository extends CrudRepository<Emoticon, String> {

	List<Emoticon> findAll();
}
