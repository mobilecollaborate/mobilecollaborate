package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Comment;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {

	List<Comment> findByTypeAndTypeId(String type, Integer typeId);
	List<Comment> findByTypeAndTypeIdOrderByCreatedAtAsc(String type, Integer typeId, Pageable pageable);
	
	Integer countByTypeAndTypeId(String type, Integer typeId);
}
