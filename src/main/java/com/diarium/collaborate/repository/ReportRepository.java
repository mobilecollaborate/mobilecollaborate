package com.diarium.collaborate.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Report;

@Repository
public interface ReportRepository extends CrudRepository<Report, Integer> {

}
