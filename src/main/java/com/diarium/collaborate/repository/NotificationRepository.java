package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Notification;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Integer> {

	@Query(value = "select * from smartbook.tb_notifications as tbn " + 
			"where tbn.to_user_id = :userId " + 
			"and (tbn.content <> '' or tbn.title <> '') " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Notification> findByToUserId(@Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	
	Integer countByToUserId(Integer toUserId);
}
