package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Connection;

@Repository
public interface ConnectionRepository extends CrudRepository<Connection, Integer> {

	@Query(value = "select * from smartbook.tb_connections where "
			+ "(smartbook.tb_connections.user_id = :userId or smartbook.tb_connections.to_user_id = :userId) "
			+ "and smartbook.tb_connections.way = 2 "
			+ "and smartbook.tb_connections.confirmed = 1 "
			+ "order by smartbook.tb_connections.created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Connection> findFriends(@Param("userId") Integer userId, @Param("limit") Integer limit, @Param("offset") Integer offset);
	@Query(value = "select * from smartbook.tb_connections where "
			+ "smartbook.tb_connections.to_user_id = :userId "
			+ "and smartbook.tb_connections.way = 2 "
			+ "and smartbook.tb_connections.confirmed = 0 "
			+ "order by smartbook.tb_connections.created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Connection> findFriendRequests(@Param("userId") Integer userId, @Param("limit") Integer limit, @Param("offset") Integer offset);
	@Query(value = "select * from smartbook.tb_connections where "
			+ "smartbook.tb_connections.user_id = :userId "
			+ "and smartbook.tb_connections.way = 1 "
			+ "order by smartbook.tb_connections.created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Connection> findFollowings(@Param("userId") Integer userId, @Param("limit") Integer limit, @Param("offset") Integer offset);
	@Query(value = "select * from smartbook.tb_connections where "
			+ "smartbook.tb_connections.to_user_id = :userId "
			+ "and smartbook.tb_connections.way = 1 "
			+ "order by smartbook.tb_connections.created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Connection> findFollowers(@Param("userId") Integer userId, @Param("limit") Integer limit, @Param("offset") Integer offset);
	
	@Query(value = "select count(smartbook.tb_connections.id) from smartbook.tb_connections where "
			+ "(smartbook.tb_connections.user_id = :userId or smartbook.tb_connections.to_user_id = :userId) "
			+ "and smartbook.tb_connections.way = 2 "
			+ "and smartbook.tb_connections.confirmed = 1;", nativeQuery = true)
	Integer countFriends(@Param("userId") Integer userId);
	@Query(value = "select count(smartbook.tb_connections.id) from smartbook.tb_connections where "
			+ "smartbook.tb_connections.to_user_id = :userId "
			+ "and smartbook.tb_connections.way = 2 "
			+ "and smartbook.tb_connections.confirmed = 0;", nativeQuery = true)
	Integer countFriendRequests(@Param("userId") Integer userId);
	@Query(value = "select count(smartbook.tb_connections.id) from smartbook.tb_connections where "
			+ "smartbook.tb_connections.user_id = :userId "
			+ "and smartbook.tb_connections.way = 1;", nativeQuery = true)
	Integer countFollowings(@Param("userId") Integer userId);
	@Query(value = "select count(smartbook.tb_connections.id) from smartbook.tb_connections where "
			+ "smartbook.tb_connections.to_user_id = :userId "
			+ "and smartbook.tb_connections.way = 1;", nativeQuery = true)
	Integer countFollowers(@Param("userId") Integer userId);

	List<Connection> findByUserIdOrToUserIdAndWayAndConfirmed(Integer userId, Integer toUserId, Integer way, Integer confirmed, Pageable pageable);
	List<Connection> findByUserIdAndWayAndConfirmed(Integer userId, Integer way, Integer confirmed, Pageable pageable);
	List<Connection> findByToUserIdAndWayAndConfirmed(Integer toUserId, Integer way, Integer confirmed, Pageable pageable);
	Connection findOneByUserIdAndToUserIdAndWayAndConfirmed(Integer userId, Integer toUserId, Integer way, Integer confirmed);
	Connection findOneByUserIdAndToUserIdAndWay(Integer userId, Integer toUserId, Integer way);
	
	Integer countByUserIdOrToUserIdAndWayAndConfirmed(Integer userId, Integer toUserId, Integer way, Integer confirmed);
	Integer countByToUserIdAndWayAndConfirmed(Integer userId, Integer way, Integer confirmed);
	Integer countByUserIdAndWayAndConfirmed(Integer userId, Integer way, Integer confirmed);
}
