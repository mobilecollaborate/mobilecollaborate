package com.diarium.collaborate.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.PostHide;

@Repository
public interface PostHideRepository extends CrudRepository<PostHide, Integer> {

}
