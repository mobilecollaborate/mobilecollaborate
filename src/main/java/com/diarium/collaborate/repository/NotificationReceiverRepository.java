package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.NotificationReceiver;

@Repository
public interface NotificationReceiverRepository extends CrudRepository<NotificationReceiver, Integer> {

	List<NotificationReceiver> findByTypeAndTypeId(String type, Integer typeId);
	NotificationReceiver findOneByUserIdAndTypeAndTypeId(Integer userId, String type, Integer typeId);
}
