package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.CommunityMember;

@Repository
public interface CommunityMemberRepository extends CrudRepository<CommunityMember, Integer> {

	List<CommunityMember> findByCommunityId(Integer communityId);
}
