package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Hashtag;

@Repository
public interface HashtagRepository extends CrudRepository<Hashtag, Integer> {

	List<Hashtag> findTop10ByOrderByCountDesc();
	Hashtag findOneByHash(String hash);
}
