package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Photo;

@Repository
public interface PhotoRepository extends CrudRepository<Photo, Integer> {

	List<Photo> findByUserId(Integer userId, Pageable pageable);
	List<Photo> findByPostId(Integer postId);
	
	Integer countByUserId(Integer userId);
}
