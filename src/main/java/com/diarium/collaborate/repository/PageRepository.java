package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Page;

@Repository
public interface PageRepository extends CrudRepository<Page, Integer> {

	@Query(value = "select * from ( " + 
			"select distinct tbp.* from smartbook.tb_pages as tbp inner join smartbook.tb_likes as tbl " + 
			"	on tbp.id = tbl.type_id " + 
			"where tbl.user_id = :userId " + 
			"	and tbl.type = 'page' ) as p " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Page> findLikedPage(@Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
}
