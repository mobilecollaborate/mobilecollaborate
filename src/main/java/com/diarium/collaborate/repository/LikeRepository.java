package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Like;

@Repository
public interface LikeRepository extends CrudRepository<Like, Integer> {

	List<Like> findByTypeAndTypeId(String type, Integer typeId);
	List<Like> findByTypeAndTypeIdOrderByCreatedAtDesc(String type, Integer typeId, Pageable pageable);
	Like findByUserIdAndTypeAndTypeId(Integer userId, String type, Integer typeId);
	
	Integer countByTypeAndTypeId(String type, Integer typeId);
	Integer countByTypeAndTypeIdAndEmoji(String type, Integer typeId, Integer emoji);
}
