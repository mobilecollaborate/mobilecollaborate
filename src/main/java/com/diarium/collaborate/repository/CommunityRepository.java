package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Community;

@Repository
public interface CommunityRepository extends CrudRepository<Community, Integer> {

	@Query(value = "select * from ( " + 
			"select distinct tbc.* from smartbook.tb_communities as tbc inner join smartbook.tb_community_members as tbcm " + 
			"	on tbc.id = tbcm.community_id " + 
			"where tbcm.user_id = :userId ) as c " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Community> findMyCommunity(@Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
}
