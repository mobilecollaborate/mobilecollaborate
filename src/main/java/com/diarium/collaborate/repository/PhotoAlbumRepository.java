package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.PhotoAlbum;

@Repository
public interface PhotoAlbumRepository extends CrudRepository<PhotoAlbum, Integer> {

	List<PhotoAlbum> findByUserId(Integer userId, Pageable pageable);
	PhotoAlbum findOneByUserIdAndTitle(Integer userId, String title);
	
	Integer countByUserId(Integer userId);
}
