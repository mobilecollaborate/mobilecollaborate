package com.diarium.collaborate.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.BirthdayCard;

@Repository
public interface BirthdayCardRepository extends CrudRepository<BirthdayCard, Integer> {

	BirthdayCard findOneByUserIdAndToUserIdAndYear(Integer userId, Integer toUserId, Integer year);
	BirthdayCard findOneByPostId(Integer postId);
}
