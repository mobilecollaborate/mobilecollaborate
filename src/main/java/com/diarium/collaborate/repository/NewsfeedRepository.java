package com.diarium.collaborate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diarium.collaborate.model.entity.Newsfeed;

@Repository
public interface NewsfeedRepository extends CrudRepository<Newsfeed, Integer> {

	@Query(value = "" +
			"select * from ( " + 
//			"-- requested friend posts " + 
			"	select distinct tbp.* from smartbook.tb_posts as tbp inner join smartbook.tb_connections as tbc " + 
			"		on tbp.user_id = tbc.to_user_id " + 
			"	where " + 
			"		( tbc.user_id = :userId ) " + 
			"		and tbc.confirmed = 1 " + 
			"		and tbc.way = 1 " + 
			"		and tbp.type = 'user-timeline' " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and ( " + 
			"			tbp.privacy = 1 " + 
			"			or tbp.privacy = 2 " + 
			"			or tbp.privacy = 4 " + 
			"		) " + 
			"	union " + 
//			"-- accepted friend posts " + 
			"	select distinct tbp.* from smartbook.tb_posts as tbp inner join smartbook.tb_connections as tbc " + 
			"		on tbp.user_id = tbc.user_id " + 
			"	where " + 
			"		( tbc.to_user_id = :userId ) " + 
			"		and tbc.confirmed = 1 " + 
			"		and tbc.way = 2 " + 
			"		and tbp.type = 'user-timeline' " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and ( " + 
			"			tbp.privacy = 1 " + 
			"			or tbp.privacy = 2 " + 
			"			or tbp.privacy = 4 " + 
			"		) " + 
			"	union " + 
//			"-- follower posts " + 
			"	select tbp.* from smartbook.tb_posts as tbp inner join smartbook.tb_connections as tbc " + 
			"		on tbp.user_id = tbc.to_user_id " + 
			"	where " + 
			"		tbc.user_id = :userId " + 
			"		and tbc.way = 1 " + 
			"		and tbp.type = 'user-timeline' " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and ( " + 
			"			tbp.privacy = 1 " + 
			"			or tbp.privacy = 3 " + 
			"			or tbp.privacy = 4 " + 
			"		) " + 
			"	union " + 
//			"-- page posts " + 
			"	select tbp.* from smartbook.tb_posts as tbp inner join smartbook.tb_likes as tbl " + 
			"		on tbp.page_id = tbl.type_id " + 
			"	where " + 
			"		tbl.type = 'page' " + 
			"		and tbl.user_id = :userId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"	union " + 
//			"-- addressed posts " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.to_user_id = :userId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"	union " + 
//			"-- my post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.user_id = :userId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			") as p where p.id not in  ( " + 
//			"-- exclude hidden post " + 
			"	select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"		on tbp.id = tbph.post_id " + 
			"	where tbph.user_id = :userId " + 
			") " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findForDashboard(@Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "" +
			"select * from ( " + 
//			"-- addressed posts " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.to_user_id = :userId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and tbp.id not in  ( " + 
//			"-- exclude hidden post " + 
			"			select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"				on tbp.id = tbph.post_id " + 
			"			where tbph.user_id = :userId " + 
			"		) " + 
			"	union " + 
//			"-- my post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.user_id = :userId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			") as p " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findForMyTimeline(@Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "" +
			"select * from ( " + 
//			"-- friend post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		(tbp.user_id = :ownerId or tbp.to_user_id = :ownerId ) " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and ( " + 
			"			tbp.privacy = 1 " + 
			"			or tbp.privacy = 2 " + 
			"			or tbp.privacy = 4 " + 
			"		) " + 
//			"-- exclude hidden post " + 
			"		and tbp.id not in  ( " + 
			"			select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"				on tbp.id = tbph.post_id " + 
			"			where tbph.user_id = :userId " + 
			"		) " + 
			"	union " + 
//			"-- my post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.user_id = :userId " + 
			"		and tbp.to_user_id = :ownerId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			") as p " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findForFriendsTimeline(@Param("ownerId") Integer ownerId, @Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "" +
			"select * from ( " + 
//			"-- following post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.user_id = :ownerId or tbp.to_user_id = :ownerId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and ( " + 
			"			tbp.privacy = 1 " + 
			"			or tbp.privacy = 3 " + 
			"			or tbp.privacy = 4 " + 
			"		) " + 
//			"-- exclude hidden post " + 
			"		and tbp.id not in  ( " + 
			"			select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"				on tbp.id = tbph.post_id " + 
			"			where tbph.user_id = :userId " + 
			"		) " + 
			") as p " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findForFollowingsTimeline(@Param("ownerId") Integer ownerId, @Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "" +
			"select * from ( " + 
//			"-- following post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.user_id = :ownerId or tbp.to_user_id = :ownerId " + 
			"		and tbp.content_type not in ('auto-post') " + 
			"		and tbp.privacy = 1 " + 
//			"-- exclude hidden post " + 
			"		and tbp.id not in  ( " + 
			"			select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"				on tbp.id = tbph.post_id " + 
			"			where tbph.user_id = :userId " + 
			"		) " + 
			") as p " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findForOthersTimeline(@Param("ownerId") Integer ownerId, @Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "" +
			"select * from ( " + 
//			"-- following post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.page_id = :pageId " + 
			"		and tbp.content_type not in ('auto-post') " + 
//			"-- exclude hidden post " + 
			"		and tbp.id not in  ( " + 
			"			select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"				on tbp.id = tbph.post_id " + 
			"			where tbph.user_id = :userId " + 
			"		) " + 
			") as p " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findByPageId(@Param("pageId") Integer pageId, @Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	@Query(value = "" +
			"select * from ( " + 
//			"-- following post " + 
			"	select tbp.* from smartbook.tb_posts as tbp " + 
			"	where " + 
			"		tbp.community_id = :communityId " + 
			"		and tbp.content_type not in ('auto-post') " + 
//			"-- exclude hidden post " + 
			"		and tbp.id not in  ( " + 
			"			select tbp.id from smartbook.tb_posts as tbp inner join smartbook.tb_post_hide as tbph " + 
			"				on tbp.id = tbph.post_id " + 
			"			where tbph.user_id = :userId " + 
			"		) " + 
			") as p " + 
//			"-- final touch " + 
			"order by created_at desc limit :offset , :limit ;", nativeQuery = true)
	List<Newsfeed> findByCommunityId(@Param("communityId") Integer communityId, @Param("userId") Integer userId, @Param("offset") Integer offset, @Param("limit") Integer limit);
	List<Newsfeed> findBySharedId(Integer sharedId);
}
